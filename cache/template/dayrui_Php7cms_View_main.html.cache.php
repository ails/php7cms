<?php if ($fn_include = $this->_include("header.html")) include($fn_include); ?>


<div class="row">
    <div class="col-md-12">
        <div class="admin-usermenu">
            <?php if (is_array($admin['usermenu'])) { $count=count($admin['usermenu']);foreach ($admin['usermenu'] as $t) { ?>
            <a class="btn <?php if ($t['color'] && $t['color']!='default') {  echo $t['color'];  } else { ?>btn-default<?php } ?>" href="<?php echo $t['url']; ?>"> <?php echo $t['name']; ?> </a>
            <?php } } ?>
        </div>
    </div>
</div>

<div class="row">



    <div class="col-md-6 col-sm-6">

        <div class="portlet light bordered myportlet">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="fa fa-bar-chart"></i>
                    <span class="caption-subject"> <?php echo dr_lang('数据统计'); ?> </span>
                </div>
            </div>
            <div class="portlet-body">
                <?php


		$mtotal = [];
		$module = \Phpcmf\Service::C()->get_cache('module-'.SITE_ID.'-content');
                if ($module) {
                foreach ($module as $dir => $t) {
                // 判断权限
                $mtotal[$dir] = [
                'name' => dr_lang($t['name']),
                'today' => \Phpcmf\Service::M('auth')->_menu_link_url($dir.'/home/index', $dir.'/home/index'),
                'all' => \Phpcmf\Service::M('auth')->_menu_link_url($dir.'/home/index', $dir.'/home/index'),
                'verify' => \Phpcmf\Service::M('auth')->_menu_link_url($dir.'/verify/index', $dir.'/verify/index'),
                'recycle' => \Phpcmf\Service::M('auth')->_menu_link_url($dir.'/home/index', $dir.'/recycle/index'),
                'timing' => \Phpcmf\Service::M('auth')->_menu_link_url($dir.'/home/index', $dir.'/time/index'),
                ];
                }
                }
                ?>
                <div class="table-scrollable">
                    <table class="table table-mtotal table-nomargin table-bordered table-striped table-bordered table-advance">
                        <thead>
                        <tr>
                            <th><?php echo dr_lang('模块'); ?></th>
                            <th><?php echo dr_lang('总数据'); ?></th>
                            <th><?php echo dr_lang('今日'); ?></th>
                            <th><?php echo dr_lang('回收站'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (is_array($mtotal)) { $count=count($mtotal);foreach ($mtotal as $dir=>$t) { ?>
                        <tr>
                            <td><?php echo $t['name']; ?></td>
                            <td><a class="drlabel drlabel-success" href="<?php echo $t['all']; ?>" id="<?php echo $dir; ?>_all"><img src="<?php echo THEME_PATH; ?>assets/images/mloading.gif"></a></td>
                            <td><a class="drlabel drlabel-success" href="<?php echo $t['today']; ?>" id="<?php echo $dir; ?>_today"><img src="<?php echo THEME_PATH; ?>assets/images/mloading.gif"></a></td>
                            <td><a class="drlabel drlabel-important" href="<?php echo $t['recycle']; ?>" id="<?php echo $dir; ?>_recycle"><img src="<?php echo THEME_PATH; ?>assets/images/mloading.gif"></a></td>
                        </tr>
                        <script type="text/javascript">
                            $(function(){
                                $.getScript("<?php echo dr_url('api/mtotal'); ?>&dir=<?php echo $dir; ?>");
                            });
                        </script>
                        <?php } } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>



    </div>


    <div class="col-md-6 col-sm-6">

        <div class="portlet light bordered myportlet ">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="fa fa-cloud"></i>
                    <span class="caption-subject"> <a style="color:#666" href="http://www.php7cms.com" target="_blank"><?php echo dr_lang('PHP7CMS'); ?></a> </span>
                </div>
            </div>
            <div class="portlet-body">

                <div class="scroller" data-always-visible="1" data-rail-visible="0">
                    <div class="col-md-9">
                        <ul class="use-info">
                            <li style="height: 35px;">
                                <span>程序版本：</span>
                                <a target="_blank" href="http://www.php7cms.com/">PHP7CMS免费版 v<?php echo $cms_version; ?></a>
                            </li>
                            <li style="height: 35px;">
                                <span>源码仓库：</span>
                                <a target="_blank" href="https://gitee.com/wchunjie/php7cms">https://gitee.com/wchunjie/php7cms</a>
                            </li>

                            <li style="height: 35px;">
                                <span>技术论坛：</span>
                                <a target="_blank" href="http://www.finebug.com/phpcmf">http://www.finebug.com/phpcmf</a>
                            </li>
                            <li style="height: 35px;">
                                <span>QQ群讨论：</span>
                                <a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=58824469df9cacea494e0ebad5e9dc69a2e4aded6b8f37ae28adb3f705c2ee9a">QQ群号 644732788</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    $(function () {
        $.ajax({type: "GET",dataType:"json", url: admin_file+"?s=mall&c=api&m=count_total",
            success: function(json) {
                if (json.code) {
                    $('#dr_member_total').html(json.data.member);
                    $('#dr_goods_total').html(json.data.goods);
                    $('#dr_order_total').html(json.data.order);
                    $('#dr_mall_total').html(json.data.mall);
                }
            },
            error: function(HttpRequest, ajaxOptions, thrownError) {

            }
        });

    });
</script>

<?php if ($fn_include = $this->_include("footer.html")) include($fn_include); ?>