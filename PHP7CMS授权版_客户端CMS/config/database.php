<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 数据库配置文件
 */

return array(
    'hostname' => 'localhost',
    'username' => 'root', // 数据库账号
    'password' => 'root', // 数据库密码
    'database' => 'root', // 数据库名
    'dbprefix' => 'dr_',
);