<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title><?php echo $meta_title; ?></title>
    <meta name="keywords" content="<?php echo $meta_keywords; ?>" />
    <meta name="description" content="<?php echo $meta_description; ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo HOME_THEME_PATH; ?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo HOME_THEME_PATH; ?>css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo HOME_THEME_PATH; ?>css/jquery.bxslider.css" rel="stylesheet" />
    <script src="<?php echo HOME_THEME_PATH; ?>js/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="<?php echo HOME_THEME_PATH; ?>js/bootstrap.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo HOME_THEME_PATH; ?>js/hover-dropdown.js"></script>
    <script type="text/javascript" src="<?php echo HOME_THEME_PATH; ?>js/jquery.bxslider.js"></script>
    <script type="text/javascript" src="<?php echo HOME_THEME_PATH; ?>layer/layer.js"></script>
    <script>
        $(function(){
            $(".close").click(function(){
                $("#address").attr("value","");
            })
        })
    </script>

</head>
<body>
<div class="top hidden-xs">
    <div class="container">
        <div class="top-fl">Hi，欢迎来到<?php echo SITE_NAME; ?>！</div>
        <div class="top-fr">客服热线：<?php echo $space['phone']; ?></div>
    </div>
</div>
<header class="header-frontend">
    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img src="<?php echo $space['thumb']; ?>"/></a>
            </div>
            <div class="navbar-collapse collapse" style="height: 1px;">
                <ul class="nav navbar-nav">
                    <li class="<?php if ($indexc) { ?>active<?php } ?>"><a href="<?php echo SITE_URL; ?>">首页</a></li>
                    <?php $list_return = $this->list_tag("action=category module=share pid=0"); if ($list_return) extract($list_return, EXTR_OVERWRITE); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { ?>
                    <?php if ($t['child']) { ?>
                    <li class="dropdown <?php if (in_array($catid, $t['catids'])) { ?>active<?php } ?>">
                        <a href="<?php echo $t['url']; ?>" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false"><?php echo $t['name']; ?> <b class=" icon-angle-down"></b></a>
                        <ul class="dropdown-menu">
                            <?php $list_return_t2 = $this->list_tag("action=category pid=$t[id]  return=t2"); if ($list_return_t2) extract($list_return_t2, EXTR_OVERWRITE); $count_t2=count($return_t2); if (is_array($return_t2)) { foreach ($return_t2 as $key_t2=>$t2) { ?>
                            <li lass="<?php if (in_array($catid, $t2['catids'])) { ?>active<?php } ?>"><a href="<?php echo $t2['url']; ?>"><?php echo $t2['name']; ?></a></li>
                            <?php } } ?>
                        </ul>
                    </li>
                    <?php } else { ?>
                    <li class="<?php if (in_array($catid, $t['catids'])) { ?>active<?php } ?>"><a href="<?php echo $t['url']; ?>"><?php echo $t['name']; ?></a></li>
                    <?php } ?>
                    <?php } } ?>
                </ul>
            </div>
        </div>
    </div>
</header>
<!--图片轮播-->
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <?php if (is_array($space['banner'])) { $count=count($space['banner']);foreach ($space['banner'] as $i=>$t) { ?>
        <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if ($i==0) { ?>active<?php } ?>"></li>
        <?php } } ?>
    </ol>
    <div class="carousel-inner" role="listbox">
        <?php if (is_array($space['banner'])) { $count=count($space['banner']);foreach ($space['banner'] as $i=>$t) { ?>
        <div class="item <?php if ($i==0) { ?>active<?php } ?>">
            <img src="<?php echo $t['file']; ?>" alt="<?php echo $t['title']; ?>">
        </div>
        <?php } } ?>
    </div>
    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="<?php echo HOME_THEME_PATH; ?>#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>