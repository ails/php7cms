
<div class="copy  hidden-md hidden-xs"><?php echo $space['content']; ?></div>
<div class="copy hidden-lg">
    <?php echo SITE_NAME; ?>
</div>
<div style="display: none;">
    <?php echo $space['tongji']; ?>
</div>
<script src="<?php echo HOME_THEME_PATH; ?>js/common-scripts.js"></script>
<script src="<?php echo HOME_THEME_PATH; ?>js/jquery.bxslider.js"></script>
<script>
    RevSlide.initRevolutionSlider();
    $(window).load(function() {
        $('[data-zlname = reverse-effect]').mateHover({
            position: 'y-reverse',
            overlayStyle: 'rolling',
            overlayBg: '#fff',
            overlayOpacity: 0.7,
            overlayEasing: 'easeOutCirc',
            rollingPosition: 'top',
            popupEasing: 'easeOutBack',
            popup2Easing: 'easeOutBack'
        });
    });
    $(window).load(function() {
        $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider) {
                $('body').removeClass('loading');
            }
        });
    });
    jQuery(".fancybox").fancybox();
</script>

</body>
</html>
