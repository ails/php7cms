<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Liuyan extends MY_Controller {

    /**
     * 留言到服务端
     */
    public function index() {

        $post = $this->input->post('data', true);
        if (!$post['title']) {
            exit($this->_json(0, '姓名不能为空', 'title'));
        } elseif (!$post['dianhua']) {
            exit($this->_json(0, '电话不能为空', 'dianhua'));
        } elseif (!$post['neirong']) {
            exit($this->_json(0, '内容不能为空', 'neirong'));
        }

        // 验证操作间隔
        $name = 'space-liuyan-post';
        $this->load->library('session');
        $this->session->userdata($name) && $this->_json(0, '已经提交成功，请稍后再试');

        // 验证文件
        if (!file_exists($file_path = WEBPATH.'config/sync.php'))
        {
            $this->_json(0, '客户端sync.php文件不存在');
        }

        $my = require $file_path;

        $url = $my['server'].'index.php?s=space&c=sync&m=liuyan&id='.$my['id'].'&sn='.$my['sync'].'&json='.json_encode($post);
        $json = dr_catcher_data($url);

        if (!$json) {
            $this->_json(0, '服务端无法获取远程数据');
        }
        $data = json_decode($json, true);
        if (!$data) {
            $this->_json(0, '服务端JSON数据解析失败');
        }


        // 间隔30秒
        $this->session->set_tempdata($name, 1, 60);


        exit($this->_json(1, '提交成功'));
    }
}
