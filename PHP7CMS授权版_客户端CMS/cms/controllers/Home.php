<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    /**
     * 首页
     */
    public function index() {

        $this->template->assign(array(
            'indexc' => 1,
            'meta_title' => SITE_TITLE,
            'meta_keywords' => SITE_KEYWORDS,
            'meta_description' => SITE_DESCRIPTION,
        ));
        $this->template->display('index.html');
    }

    public function test() {
        exit('php7cms');
    }
}
