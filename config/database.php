<?php

/**
 * 数据库配置文件
 */

$db['default']	= [
    'hostname'	=> 'localhost',
    'username'	=> 'root',
    'password'	=> 'mysql',
    'database'	=> 'php7cms',
    'DBPrefix'	=> 'dr_',
];