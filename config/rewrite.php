<?php

/**
 * URL解析规则
 * 例如：  114.html 对应 index.php?s=demo&c=show&id=114
 * 可以解析：  "114.html"  => 'index.php?s=demo&c=show&id=114',
 * 动态id解析：  "([0-9]+).html"  => 'index.php?s=demo&c=show&id=$1',
 */

return [

    /***********************系统默认URL解析规则*************************/


    "demo"  => 'index.php?s=demo',
    "demo\/([\w]+)\-([0-9]+).html(.*)"  => 'index.php?s=demo&c=category&dir=$1&page=$2',
    "demo\/([\w]+).html(.*)"  => 'index.php?s=demo&c=category&dir=$1',
    "demo\/([\w]+)\/([0-9]+)\-([0-9]+).html(.*)"  => 'index.php?s=demo&c=show&id=$2&page=$3',
    "demo\/([\w]+)\/([0-9]+).html(.*)"  => 'index.php?s=demo&c=show&id=$2',


    "tag\/(.+).html(.*)"  => 'index.php?c=tag&name=$1',


    "([a-z]+)\/search.html(.*)"  => 'index.php?s=$1&c=search',
    "([a-z]+)\/search\/(.+).html(.*)"  => 'index.php?s=$1&c=search&rewrite=$2',

    "([\w]+)\/([0-9]+)\-([0-9]+).html(.*)" => 'index.php?c=show&id=$2&page=$3',
    "([\w]+)\/([0-9]+).html(.*)"  => 'index.php?c=show&id=$2',
    "([\w]+)\-([0-9]+).html(.*)"  => 'index.php?c=category&dir=$1&page=$2',
    "([\w]+).html(.*)"  => 'index.php?c=category&dir=$1',

];