<?php namespace Phpcmf\Controllers;

define('IS_INSTALL', 1);

// 安装程序
class Install extends \Phpcmf\Common
{

    public $db;
    private $lock;

    /**
     * 初始化共享控制器
     */
    public function __construct(...$params)
    {
        parent::__construct(...$params);
        $this->lock = WRITEPATH.'install.lock';
        is_file($this->lock) && exit('安装程序已经被锁定，重新安装请删除：cache/install.lock');
        if (version_compare(PHP_VERSION, '7.1.0') < 0) {
            echo "<font color=red>PHP版本必须在7.1以上</font>";exit;
        }

        define('SITE_LANGUAGE', 'zh-cn');
        define('SITE_ID', 1);
        define('IS_API_HTTP', 0);
        define('THEME_PATH', '/static/');
        define('LANG_PATH', '/config/language/'.SITE_LANGUAGE.'/'); // 语言包

        $app = require MYPATH.'Config/Version.php';
        define('DR_NAME', $app['name']);
        define('DR_VERSION', $app['version']);
        \Phpcmf\Service::V()->admin();
    }

    public function index() {

        $step = intval($_GET['step']);

        switch ($step) {

            case 0:

                // 安装信息填写
                if (IS_AJAX_POST) {

                    $data = $_POST['data'];
                    if (empty($data['db_host'])) {
                        $this->_json(0, '数据库地址不能为空');
                    } elseif (empty($data['db_user'])) {
                        $this->_json(0, '数据库账号不能为空');
                    } elseif (empty($data['db_name'])) {
                        $this->_json(0, '数据库名称不能为空');
                    } elseif (is_numeric($data['db_name'])) {
                        exit($this->_json(0, '数据库名称不能是数字'));
                    } elseif (strpos($data['db_name'], '.') !== false) {
                        exit($this->_json(0, '数据库名称不能存在.号'));
                    }

                    $mysqli = function_exists('mysqli_init') ? mysqli_init() : 0;
                    if (!$mysqli) {
                        exit($this->_json(0, '您的PHP环境必须启用Mysqli扩展'));
                    }
                    if (!@mysqli_real_connect($mysqli, $data['db_host'], $data['db_user'], $data['db_pass'])) {
                        exit($this->_json(0, '[mysqli_real_connect] 无法连接到数据库服务器（'.$data['db_host'].'），请检查用户名（'.$data['db_user'].'）和密码（'.$data['db_pass'].'）是否正确'));
                    }
                    if (!@mysqli_select_db($mysqli, $data['db_name'])) {
                        if (!@mysqli_query($mysqli, 'CREATE DATABASE '.$data['db_name'])) {
                            exit($this->_json(0, '指定的数据库（'.$data['db_name'].'）不存在，系统尝试创建失败，请通过其他方式建立数据库'));
                        }
                    }

                    // 存储缓存文件中
                    $size = @file_put_contents(WRITEPATH.'install.info', dr_array2string($data));
                    if (!$size || $size < 10) {
                        $this->_json(0, '临时数据存储失败，cahce目录无法写入');
                    }

                    // 存储mysql
                    $database = '<?php

/**
 * 数据库配置文件
 */

$db[\'default\']	= [
    \'hostname\'	=> \''.$data['db_host'].'\',
    \'username\'	=> \''.$data['db_user'].'\',
    \'password\'	=> \''.$data['db_pass'].'\',
    \'database\'	=> \''.$data['db_name'].'\',
    \'DBPrefix\'	=> \'dr_\',
];';
                    $size = @file_put_contents(WEBPATH.'config/database.php', $database);
                    if (!$size || $size < 10) {
                        $this->_json(0, '数据库配置文件创建失败，config目录无法写入');
                    }

                    $this->_json(1, 'index.php?c=install&m=index&step='.($step+1));
                }


                break;

            case 1:

                $error = '';
                $data = dr_string2array(file_get_contents(WRITEPATH.'install.info'));
                file_put_contents(WRITEPATH.'install.error', '');
                if (empty($data)) {
                    $error = '临时数据获取失败，请返回前一页重新执行';
                } else {
                    $this->db = \Config\Database::connect('default');
                    // 检查数据库是否存在
                    if (!$this->db->connect(false)) {
                        // 链接失败,尝试创建数据库
                        $error = '数据库连接失败，请返回前一页重新执行';
                    } else {
                        // 导入默认安装数据
                        $sql = file_get_contents(CMSPATH.'Config/Install.sql');
                        $sql = str_replace('{site_url}', DOMAIN_NAME, $sql);
                        $this->query($sql);
                        $errorlog = file_get_contents(WRITEPATH.'install.error');
                        if (strlen($errorlog) > 10) {
                            // 出现错误了
                            $error = $errorlog;
                        } else {


                            // 写配置文件
                            $sys = [
                                'SYS_DEBUG'                     => '0', //调试器开关
                                'SYS_ADMIN_CODE'                => '0', //后台登录验证码开关
                                'SYS_ADMIN_LOG'                 => '0', //后台操作日志开关
                                'SYS_AUTO_FORM'                 => '0', //自动存储表单数据
                                'SYS_ADMIN_PAGESIZE'            => '10', //后台数据分页显示数量
                                'SYS_CAT_RNAME'                 => '0', //栏目目录允许重复
                                'SYS_PAGE_RNAME'                => '0', //单页目录允许重复
                                'SYS_KEY'                       => 'PHPCMF'.SYS_TIME, //安全密匙
                                'SYS_HTTPS'                     => '0', //https模式
                                'SYS_ATTACHMENT_DB'             => '', //附件归属开启模式
                                'SYS_ATTACHMENT_PATH'           => '', //附件上传路径
                                'SYS_ATTACHMENT_URL'            => '', //附件访问地址
                                'SYS_EMAIL' => $data['email'],
                            ];
                            \Phpcmf\Service::M('System')->save_config($sys, $sys);



                            // 完成之后更新缓存
                            \Phpcmf\Service::M('cache')->update_cache();
                            $errorlog = file_get_contents(WRITEPATH.'install.error');
                            if (count($errorlog) > 10) {
                                // 出现错误了
                                $error = $errorlog;
                            } else {
                                // 安装完成
                                file_put_contents($this->lock, time());
                            }
                        }
                    }
                }

                break;


        }
        \Phpcmf\Service::V()->assign([
            'step' => $step,
            'error' => $error,
            'pre_url' => 'index.php?c=install&m=index&step='.($step-1),
            'next_url' => 'index.php?c=install&m=index&step='.($step+1),
        ]);
        \Phpcmf\Service::V()->display('install/'.$step.'.html');
        exit;
    }


    // 数据执行
    private function query($sql) {

        if (!$sql) {
            return '';
        }

        $sql_data = explode(';SQL_FINECMS_EOL', trim(str_replace(array(PHP_EOL, chr(13), chr(10)), 'SQL_FINECMS_EOL', $sql)));

        foreach($sql_data as $query){
            if (!$query) {
                continue;
            }
            $ret = '';
            $queries = explode('SQL_FINECMS_EOL', trim($query));
            foreach($queries as $query) {
                $ret.= $query[0] == '#' || $query[0].$query[1] == '--' ? '' : $query;
            }
            if (!$ret) {
                continue;
            }
            if (!$this->db->simpleQuery($ret)) {
                $rt = $this->db->error();
                $error = '**************************************************************************'
                    .PHP_EOL.$ret.PHP_EOL.$rt['message'].PHP_EOL;
                $error.= '**************************************************************************'.PHP_EOL;
                file_put_contents(WRITEPATH.'install.error', $error.PHP_EOL, FILE_APPEND);
            }
        }
    }

}
