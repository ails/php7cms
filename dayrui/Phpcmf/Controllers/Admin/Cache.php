<?php namespace Phpcmf\Controllers\Admin;

// 缓存更新
class Cache extends \Phpcmf\Common
{

    public function index() {

        \Phpcmf\Service::V()->assign([
            'list' => [
                ['系统配置缓存', 'update_cache'],
                ['数据查询缓存', 'update_data_cache'],
                ['重建搜索索引', 'update_search_index'],
                ['初始化后台菜单', 'update_site_config'],
            ],
            'menu' => \Phpcmf\Service::M('auth')->_admin_menu(
                [
                    '系统更新' => [\Phpcmf\Service::L('Router')->class.'/index', 'fa fa-refresh'],
                    'help' => [378],
                ]
            )
        ]);
        \Phpcmf\Service::V()->display('cache.html');
    }



}
