<?php namespace Phpcmf\Controllers\Admin;

class Home extends \Phpcmf\Common
{

	public function home() {
		$this->index();
	}

	public function main() {
        $cms = require MYPATH.'Config/Version.php';
        \Phpcmf\Service::V()->assign([
            'menu' => \Phpcmf\Service::M('auth')->_admin_menu(
                [
                    SITE_NAME ? SITE_NAME : '后台' => ['home/main', 'fa fa-home'],
                ]
            ),
            'color' => ['blue', 'red', 'green', 'dark', 'yellow'],
            'cms_version' => $cms['version'],
        ]);
		\Phpcmf\Service::V()->display('main.html');exit;
	}

	public function index() {

        $menu_top = $my_menu = [];

        $m = \Phpcmf\Service::M('menu')->cache();
        $menu = $m['admin'];
        $my_menu = $menu;

		$first = 0;
		$string = '';
        $mstring = '';

        if ($my_menu) {
            foreach ($my_menu as $tid => $top) {
                if (!$top['left']) {
                    continue; // 没有分组菜单就不要
                }
                $_left = 0; // 是否第一个分组菜单，0表示第一个
                $_link = 0; // 是否第一个链接菜单，0表示第一个
                $left_string = '';
                $mleft_string = [];
                !$first && $first = $tid;
                foreach ($top['left'] as $if => $left) {
                    if (!$left['link']) {
                        unset($top['left'][$if]);
                        continue; // 没有链接菜单就不要
                    }
                    // 链接菜单开始
                    $link_string = '';
                    $mlink_string = '';
                    foreach ($left['link'] as $i => $link) {

                        $url = $link['url'] ? $link['url'] :\Phpcmf\Service::L('Router')->url($link['uri']);
                        if (!$_link) {
                            // 第一个链接菜单时 指定class
                            $class = 'nav-item active open';
                            $top['url'] = $url;
                            $top['link_id'] = $link['id'];
                            $top['left_id'] = $left['id'];
                        } else {
                            $class = 'nav-item';
                        }
                        $_link = 1; // 标识以后的菜单就不是第一个了
                        $link['icon'] = $link['icon'] ? $link['icon'] : 'fa fa-th-large';
                        $link_string.= '<li id="dr_menu_link_'.$link['id'].'" class="'.$class.'"><a href="javascript:Mlink('.$tid.', '.$left['id'].', '.$link['id'].', \''.$url.'\');"><i class="iconm '.$link['icon'].'"></i> <span class="title">'.dr_lang($link['name']).'</span></a></li>';
                        $mlink_string.= '<li id="dr_menu_m_link_'.$link['id'].'" class="'.$class.'"><a href="javascript:Mlink('.$tid.', '.$left['id'].', '.$link['id'].', \''.$url.'\');"><i class="iconm '.$link['icon'].'"></i> <span class="title">'.dr_lang($link['name']).'</span></a></li>';
                    }
                    if (!$link_string) {
                        continue; // 没有链接菜单就不要
                    }
                    $left_string.= '
				<li id="dr_menu_left_'.$left['id'].'" class="dr_menu_'.$tid.' dr_menu_item nav-item '.($_left ? '' : 'active open').' " style="'.($first==$tid ? '' : 'display:none').'">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="'.$left['icon'].'"></i>
                        <span class="title">'.dr_lang($left['name']).'</span>
                        <span class="selected" style="'.($_left ? 'display:none' : '').'"></span>
                        <span class="arrow '.($_left ? '' : ' open').'"></span>
                    </a>
					<ul class="sub-menu">'.$link_string.'</ul>
				</li>';
                    $mleft_string[] = $mlink_string;
                    $_left = 1; // 标识以后的菜单就不是第一个了
                }
                if (!$left_string) {
                    $first == $tid && $first = 0;
                    continue; // 没有分组菜单就不要
                }
                $string.= $left_string;

                $mstring.= '<li class="dropdown dropdown-extended dropdown-tasks fc-mb-sum-menu" id="dr_m_top_'.$tid.'" style=" '.($first == $tid ? '' : 'display:none').'">
                    <a  class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"> 
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        '.implode($mleft_string, '<li class="divider"> </li>').'
                    </ul>
                </li>';
                unset($top['left']);
                $menu_top[$tid] = $top;
            }
        }

		\Phpcmf\Service::V()->assign([
			'top' => $menu_top,
			'first' => $first,
			'string' => $string,
			'mstring' => $mstring,
            'is_mobile' => $this->_is_mobile(),
			'sys_color' => [
				'default' => '#333438',
				'blue' => '#368ee0',
				'darkblue' => '#2b3643',
				'grey' => '#697380',
				'light' => '#F9FAFD',
				'light2' => '#F1F1F1',
			]
        ]);
		\Phpcmf\Service::V()->display('index.html');exit;
	}
}
