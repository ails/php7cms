<?php namespace Phpcmf\Controllers\Admin;

class Module_search extends \Phpcmf\Common
{
	public function index() {

        $module = \Phpcmf\Service::M('Module')->get_module_info();

        if (IS_AJAX_POST) {

            $data = \Phpcmf\Service::L('Input')->post('data', true);
            foreach ($data as $dir => $t) {
                $module[$dir]['setting']['search'] = $t;
                \Phpcmf\Service::M()->db->table('module')->where('dirname', $dir)->update([
                    'setting' => dr_array2string($module[$dir]['setting']),
                ]);
            }

            $this->_json(1, dr_lang('操作成功'));
        }

        $page = \Phpcmf\Service::L('Input')->get('page');
        if (!$page) {
            $one = reset($module);
            $page = $one['dirname'];
        }
        
        foreach ($module as $i => $t) {
            
            $module[$i]['search_field'] = [
                'keyword' => dr_lang('关键词'),
                'order' => dr_lang('排序'),
                'page' => dr_lang('分页'),
            ];
            $field = \Phpcmf\Service::M()->db->table('field')
                ->where('disabled', 0)
                ->where('ismain', 1)
                ->where('relatedname', 'module')
                ->where('relatedid', $t['id'])
                ->orderBy('displayorder ASC,id ASC')
                ->get()->getResultArray();
            foreach ($field as $f) {
                $module[$i]['search_field'][$f['fieldname']] = $f['name'];
            }
        }


        \Phpcmf\Service::V()->assign([
            'menu' => \Phpcmf\Service::M('auth')->_admin_menu(
                [
                    '搜索设置' => [\Phpcmf\Service::L('Router')->class.'/index', 'fa fa-search'],
                ]
            ),
            'page' => $page,
            'form' => dr_form_hidden(['page' => $page]),
            'module' => $module
        ]);
        \Phpcmf\Service::V()->display('module_search.html');
	}

}
