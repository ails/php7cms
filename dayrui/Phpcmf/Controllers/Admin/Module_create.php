<?php namespace Phpcmf\Controllers\Admin;

class Module_create extends \Phpcmf\Common
{

    // 创建模块
    public function index() {

        if (IS_AJAX_POST) {

            $data = \Phpcmf\Service::L('Input')->post('data', true);

            // 参数判断
            !$data['name'] && $this->_json(0, dr_lang('名称不能为空'), ['field' => 'name']);
            !$data['dirname'] && $this->_json(0, dr_lang('目录不能为空'), ['field' => 'dirname']);
            !preg_match('/^[a-z]+$/i', $data['dirname']) && $this->_json(0, dr_lang('目录只能是英文字母'), ['field' => 'dirname']);
            is_dir(APPSPATH.ucfirst($data['dirname'])) && $this->_json(0, dr_lang('此目录已经存在'), ['field' => 'dirname']);
            !dr_check_put_path(APPSPATH) && $this->_json(0, dr_lang('服务器没有创建目录的权限'), ['field' => 'dirname']);

            // 开始复制到指定目录
            $path = APPSPATH.ucfirst($data['dirname']).'/';
            \Phpcmf\Service::L('File')->copy_file(FCPATH.'Temp/Module/', $path);
            !is_file($path.'Config/App.php') && $this->_json(0, dr_lang('目录创建失败，请检查文件权限'), ['field' => 'dirname']);

            // 替换模块配置文件
            $app = file_get_contents($path.'Config/App.php');
            $app = str_replace(['{name}', '{icon}'], [$data['name'], $data['icon']], $app);
            file_put_contents($path.'Config/App.php', $app);

            $this->_json(1, dr_lang('模块创建成功'));
            exit;

        }

        \Phpcmf\Service::V()->assign([
            'form' => dr_form_hidden(),
            'menu' =>  \Phpcmf\Service::M('auth')->_admin_menu([
                '创建模块' => [\Phpcmf\Service::L('Router')->class.'/index', 'fa fa-plus'],
                'help' => [24]
            ])
        ]);
        \Phpcmf\Service::V()->display('module_create.html');
    }




}
