<?php namespace Phpcmf\Controllers\Admin;

class Module_param extends \Phpcmf\Common
{

	public function index() {

        $module = \Phpcmf\Service::M('Module')->get_module_info();

        if (IS_AJAX_POST) {

            $data = \Phpcmf\Service::L('Input')->post('data', true);
            foreach ($data as $dir => $t) {
                $module[$dir]['setting']['param'] = $t;
                \Phpcmf\Service::M()->db->table('module')->where('dirname', $dir)->update([
                    'setting' => dr_array2string($module[$dir]['setting']),
                ]);
            }

            $this->_json(1, dr_lang('操作成功'));
        }

        $page = \Phpcmf\Service::L('Input')->get('page');
        if (!$page) {
            $one = reset($module);
            $page = $one['dirname'];
        }

        \Phpcmf\Service::V()->assign([
            'menu' => \Phpcmf\Service::M('auth')->_admin_menu(
                [
                    '模块参数' => [\Phpcmf\Service::L('Router')->class.'/index', 'fa fa-gears'],
                ]
            ),
            'page' => $page,
            'form' => dr_form_hidden(['page' => $page]),
            'module' => $module
        ]);
        \Phpcmf\Service::V()->display('module_param.html');
	}


}
