<?php namespace Phpcmf\Controllers;

class Home extends \Phpcmf\Common
{
	private $is_html;

	// 首页动作
	public function _index() {
		\Phpcmf\Service::V()->assign([
			'indexc' => 1,
		]);
        \Phpcmf\Service::V()->assign(\Phpcmf\Service::L('Seo')->index());
		\Phpcmf\Service::V()->display('index.html');
	}

	// 首页显示
	public function index() {

        $this->_index();
	}

	/**
	 * 404 页面
	 */
	public function s404() {
		$uri = \Phpcmf\Service::L('Input')->get('uri');
		$this->goto_404_page('没有找到这个页面: '.$uri);
	}


	// 生成静态
	public function html() {

	}

}
