<?php

/**
 * 线程任务接口
 */

$file = WRITEPATH.'thread/'.dr_safe_filename(\Phpcmf\Service::L('Input')->get('auth')).'.auth';
if (!is_file($file)) {
    log_message('error', '线程任务auth文件不存在：'.FC_NOW_URL);
    exit('线程任务auth文件不存在'.$file);
}

$time = (int)file_get_contents($file);
@unlink($file);
if (SYS_TIME - $time > 500) {
    // 500秒外无效
    log_message('error', '线程任务auth过期：'.FC_NOW_URL);
    exit('线程任务auth过期');
}

$id = intval(\Phpcmf\Service::L('Input')->get('id'));
$rt = \Phpcmf\Service::M('cron')->do_cron_id($id);
if (!$rt['code']) {
    log_message('error', '任务查询失败（'.$rt['msg'].'）：'.FC_NOW_URL);
    exit('任务查询失败（'.$rt['msg'].'）');
}


exit('ok');