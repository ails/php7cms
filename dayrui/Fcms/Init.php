<?php

// 当前URL
$pageURL = 'http';
((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')
    || (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443')) && $pageURL.= 's';
$pageURL.= '://';
if (strpos($_SERVER['HTTP_HOST'], ':') !== FALSE) {
    $url = explode(':', $_SERVER['HTTP_HOST']);
    $url[0] ? $pageURL.= $_SERVER['HTTP_HOST'] : $pageURL.= $url[0];
} else {
    $pageURL.= $_SERVER['HTTP_HOST'];
}
$pageURL.= $_SERVER['REQUEST_URI'] ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'];
define('FC_NOW_URL', $pageURL);

$startMemory = memory_get_usage();
$startTime = microtime(true);

define('BASEPATH', FCPATH.'System/');
define('CMSPATH', FCPATH.'Fcms/');
define('COREPATH', FCPATH.'Phpcmf/');
define('APPSPATH', FCPATH.'App/');
define('MYPATH', FCPATH.'Php7cms/');
define('TPLPATH', ROOTPATH.'template/');


define('IS_AJAX', (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest'));

define('IS_POST', $_SERVER['REQUEST_METHOD'] == 'POST' && count($_POST) ? TRUE : FALSE);

define('SYS_TIME', $_SERVER['REQUEST_TIME'] ? $_SERVER['REQUEST_TIME'] : time());

define('IS_AJAX_POST', IS_AJAX && IS_POST);

// 系统变量
$system = is_file(WRITEPATH.'config/system.php') ? require WRITEPATH.'config/system.php' : [
    'SYS_DEBUG'                     => '0', //调试器开关
    'SYS_ADMIN_CODE'                => '0', //后台登录验证码开关
    'SYS_ADMIN_LOG'                 => '0', //后台操作日志开关
    'SYS_AUTO_FORM'                 => '0', //自动存储表单数据
    'SYS_ADMIN_PAGESIZE'            => '10', //后台数据分页显示数量
    'SYS_CAT_RNAME'                 => '1', //栏目目录允许重复
    'SYS_PAGE_RNAME'                => '0', //单页目录允许重复
    'SYS_KEY'                       => '', //安全密匙
    'SYS_HTTPS'                     => '0', //https模式
    'SYS_ATTACHMENT_DB'             => '', //附件归属开启模式
    'SYS_ATTACHMENT_PATH'           => '', //附件上传路径
    'SYS_ATTACHMENT_URL'            => '', //附件访问地址
];
foreach ($system as $var => $value) {
    !defined($var) && define($var, $value);
}

// 缓存变量
$cache = [];
if (is_file(WRITEPATH.'config/cache.php')) {
    $cache = require WRITEPATH.'config/cache.php';
}
foreach ([
             'SYS_CACHE',
             'SYS_CACHE_SHOW',
             'SYS_CACHE_INDEX',
             'SYS_CACHE_ATTACH',
             'SYS_CACHE_LIST',
             'SYS_CACHE_SEARCH',
         ] as $name) {
    define($name, (int)$cache[$name]);
}


define('DOMAIN_NAME', strtolower($_SERVER['HTTP_HOST']));


$uu = isset($_SERVER['HTTP_X_REWRITE_URL']) || trim($_SERVER['REQUEST_URI'], '/') == SELF ? trim($_SERVER['HTTP_X_REWRITE_URL'], '/') : ($_SERVER['REQUEST_URI'] ? trim($_SERVER['REQUEST_URI'], '/') : NULL);

$uri = strpos($uu, SELF) === 0 || strpos($uu, '?') === 0 ? '' : $uu;

define('CMSURI', $uri);

if (!IS_ADMIN && $uri && !defined('IS_API')) {
	// 自定义URL解析规则
	$routes = is_file(WEBPATH.'config/rewrite.php') ? require WEBPATH.'config/rewrite.php' : [];
    $routes['rewrite-test.html'] = 'index.php?s=api&c=rewrite&m=test'; // 测试规则
    $routes['sitemap.xml'] = 'index.php?s=api&c=rewrite&m=sitemap'; // 地图规则
	// 正则匹配路由规则
	$is_404 = 1;
	foreach ($routes as $key => $val) {
		$rewrite = $match = [];
		if ($key == $uri || preg_match('/^'.$key.'$/U', $uri, $match)) {
			unset($match[0]);
			// 开始匹配
			$is_404 = 0;
			// 开始解析路由 URL参数模式
            $_GET = [];
            $queryParts = explode('&', str_replace('index.php?', '', $val));
            if ($queryParts) {
                foreach ($queryParts as $param) {
                    $item = explode('=', $param);
                    $_GET[$item[0]] = $item[1];
                    if (strpos($item[1], '$') !== FALSE) {
                        $id = (int)substr($item[1], 1);
                        $_GET[$item[0]] = isset($match[$id]) ? $match[$id] : $item[1];
                    }
                }
            }
            !$_GET['c'] && $_GET['c'] = 'home';
            !$_GET['m'] && $_GET['m'] = 'index';
			// 结束匹配
			break;
		}
	}
	// 说明是404
	if ($is_404) {
		$_GET['s'] = '';
		$_GET['c'] = 'home';
		$_GET['m'] = 's404';
		$_GET['uri'] = $uri;
	}
}

!defined('IS_API') && define('IS_API', isset($_GET['s']) && $_GET['s'] == 'api');


if (!IS_API && isset($_GET['s']) && preg_match('/^[a-z]+$/i', $_GET['s'])) {
	
	$dir = ucfirst($_GET['s']);
	if (!IS_ADMIN && $dir == 'Member') {
		// 会员
		if ($_GET['app'] && is_dir(APPSPATH.ucfirst($_GET['app']))) {
			define('APPPATH', APPSPATH.ucfirst($_GET['app']).'/');
			define('APP_DIR', strtolower($_GET['app'])); // 应用目录名称
		} else {
			define('APPPATH', COREPATH);
			define('APP_DIR', ''); // 模块目录名称
		}
		define('IS_MEMBER', TRUE);
	} elseif (is_dir(APPSPATH.$dir)) {
		define('APPPATH', APPSPATH.$dir.'/');
		define('APP_DIR', strtolower($dir)); // 应用目录名称
		define('IS_MEMBER', FALSE);
	} else {
        exit('APP目录不存在: APPSPATH/'.$dir.'/');
	}
} else {
	define('APPPATH', COREPATH);
	define('APP_DIR', '');
	define('IS_MEMBER', FALSE);
}


define('CI_DEBUG', IS_DEV ? 1 : IS_ADMIN && SYS_DEBUG);
define('TESTPATH', WRITEPATH.'tests/');

/*
 * ---------------------------------------------------------------
 * GRAB OUR CONSTANTS & COMMON
 * ---------------------------------------------------------------
 */
require COREPATH.'Config/Constants.php';

require BASEPATH.'Common.php';

/*
 * ---------------------------------------------------------------
 * LOAD OUR AUTOLOADER
 * ---------------------------------------------------------------
 *
 * The autoloader allows all of the pieces to work together
 * in the framework. We have to load it here, though, so
 * that the config files can use the path constants.
 */

require BASEPATH.'Autoloader/Autoloader.php';
require COREPATH .'Config/Autoload.php';
require COREPATH .'Config/Services.php';


// Use Config\Services as CodeIgniter\Services
class_alias('Config\Services', 'CodeIgniter\Services');

$loader = CodeIgniter\Services::autoloader();
$loader->initialize(new Config\Autoload());
$loader->register();    // Register the loader with the SPL autoloader stack.


/*
 * ---------------------------------------------------------------
 * GRAB OUR CODEIGNITER INSTANCE
 * ---------------------------------------------------------------
 *
 * The CodeIgniter class contains the core functionality to make
 * the application run, and does all of the dirty work to get
 * the pieces all working together.
 */

$app = new \Phpcmf\Extend\CodeIgniter(new \Config\App());
$app->initialize();
$app->run();