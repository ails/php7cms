<?php namespace Phpcmf;

/**
 * CMS模板标签解析
 */
class View {

    private $_is_admin; // 是否后台模板
    private $_disp_dir; // display传人的目录参数

    private $_dir; // 模板目录
    private $_tname; // 判断是否是手机端目录

    private $_cache; // 模板缓存目录

    private $_root; // 默认前端项目模板目录
    private $_mroot; // 默认会员项目模板目录

    private $_root_array; // 默认前端项目模板目录,pc+移动
    private $_mroot_array; // 默认会员项目模板目录,pc+移动
    private $_module_root_array; // 默认模块前端模板目录,pc+移动

    private $_aroot; // 默认后台项目模板目录

    private $_options; // 模板变量
    private $_filename; // 主模板名称
    private $_include_file; // 引用计数

    private $pos_order; // 是否包含有地图定位的排序

    private $action; // 指定action

    /**
     * 构造函数
     */

    public function __construct(...$params) {
        // 默认主项目模板目录
        $this->_root_array = array(
            'pc' => TPLPATH.'pc/'.SITE_TEMPLATE.'/home/',
            'mobile' => TPLPATH.'mobile/'.SITE_TEMPLATE.'/home/',
        );
        // 默认会员项目模板目录
        $this->_mroot_array = array(
            'pc' => TPLPATH.'pc/'.SITE_TEMPLATE.'/member/',
            'mobile' => TPLPATH.'mobile/'.SITE_TEMPLATE.'/member/',
        );
        // 模板缓存目录
        $this->_cache = WRITEPATH.'template/';
        // 模板选择
        $this->_tname = IS_MOBILE && is_dir(TPLPATH.'mobile/') ? 'mobile' : 'pc';

        // 当前项目模板目录
        if (IS_ADMIN) {
            // 后台
            $this->admin();
        } elseif (IS_MEMBER) {
            // 会员
            IS_MOBILE && !is_file(TPLPATH.'mobile/'.SITE_TEMPLATE.'/member/index.html') && $this->_tname = 'pc';
            $this->_root = $this->_root_array[$this->_tname];
            $this->_dir = $this->_mroot = $this->_mroot_array[$this->_tname];
            // 当用户中心没有这个模板目录时我们就调用default的用户中心模板
            !is_dir($this->_mroot) && $this->_mroot = str_replace('/'.SITE_TEMPLATE.'/', '/default/', $this->_mroot);
            // 项目的会员中心
            APP_DIR != 'member' && $this->_dir = $this->_mroot.APP_DIR.'/';
        } elseif (IS_API || (APP_DIR && is_dir(APPSPATH.ucfirst(APP_DIR).'/'))) {
            // 插件模块或者api
            $dir = IS_API ? 'api' : APP_DIR;
            IS_MOBILE && !is_file(TPLPATH.'mobile/'.SITE_TEMPLATE.'/home/index.html') && $this->_tname = 'pc';
            $this->_root = $this->_root_array[$this->_tname];
            $this->_module_root_array = array(
                'pc' => TPLPATH.'pc/'.SITE_TEMPLATE.'/home/'.$dir.'/',
                'mobile' => TPLPATH.'mobile/'.SITE_TEMPLATE.'/home/'.$dir.'/',
            );
            $this->_dir = $this->_module_root_array[$this->_tname];
        } else {
            // 首页前端页面
            IS_MOBILE && !is_file(TPLPATH.'mobile/'.SITE_TEMPLATE.'/home/index.html') && $this->_tname = 'pc';
            $this->_dir = $this->_root = $this->_root_array[$this->_tname];
        }

        // 可用action
        $this->action = [
            'category', 'module', 'content', 'related', 'share', 'table', 'form', 'mform', 'member', 'page',
            'tag', 'hits', 'search', 'category_search_field', 'linkage', 'sql',
            'cache', 'navigator'
        ];
    }


    /**
     * 强制设置为当前默认的模板目录(一般用于api外部接入)
     */
    public function dir($dir) {
        $this->_dir = $this->_mdir = TPLPATH.$dir;
    }


    /**
     * 强制设置为后台模板目录
     */
    public function admin() {
        $this->_dir = APPPATH.'Views/';
        $this->_aroot = COREPATH.'Views/';
        $this->_is_admin = 1;
    }

    /**
     * 强制设置模块模板目录
     *
     * @param	string	$dir	模板名称
     * @param	string  $isweb  强制前台
     */
    public function module($module, $isweb = 0) {

        if ($isweb == 0 && (IS_ADMIN || IS_MEMBER)) {
            return;
        }

        // 默认模板目录
        $this->_module_root_array = array(
            'pc' => TPLPATH.'pc/'.SITE_TEMPLATE.'/home/'.$module.'/',
            'mobile' => TPLPATH.'mobile/'.SITE_TEMPLATE.'/home/'.$module.'/',
        );

        $this->_root = $this->_root_array[$this->_tname];
        $this->_dir = $this->_module_root_array[$this->_tname];
    }

    /**
     * 输出模板
     *
     * @param	string	$_name		模板文件名称（含扩展名）
     * @param	string	$_dir		模块名称
     * @return  void
     */
    public function display($_name, $_dir = '') {

        extract($this->_options, EXTR_PREFIX_SAME, 'data');
        $this->_filename = $_name;

        !IS_DEV && $this->_options = null;

        // 加载编译后的缓存文件
        $this->_disp_dir = $_dir;
        $_view_file = $this->get_file_name($_name);
        $_view_name = str_replace([TPLPATH, FCPATH, APPSPATH], '', $_view_file);
        \Config\Services::timer()->start($_view_name);
        include $this->load_view_file($_view_file);
        \Config\Services::timer()->stop($_view_name);

        // 消毁变量
        $this->_include_file = null;
    }

    /**
     * 设置模块/应用的模板目录
     *
     * @param	string	$file		文件名
     * @param	string	$dir		模块/应用名称
     * @param	string	$include	是否使用的是include标签
     */
    public function get_file_name($file, $dir = null, $include = FALSE) {

        $dir = $dir ? $dir : $this->_disp_dir;

        if (IS_ADMIN || $dir == 'admin' || $this->_is_admin) {
            // 后台操作时，不需要加载风格目录，如果文件不存在可以尝试调用主项目模板

            if (APP_DIR && is_file(MYPATH.'View/'.APP_DIR.'/'.$file)) {
                return MYPATH.'View/'.APP_DIR.'/'.$file;
            } elseif (!APP_DIR && is_file(MYPATH.'View/'.$file)) {
                return MYPATH.'View/'.$file;
            } elseif (is_file($this->_dir.$file)) {
                return $this->_dir.$file; // 调用当前后台的模板
            } elseif (is_file($this->_aroot.$file)) {
                return $this->_aroot.$file; // 当前项目目录模板不存在时调用主项目的
            } elseif ($dir != 'admin' && is_file(APPSPATH.ucfirst($dir).'/Views/'.$file)) {
                return APPSPATH.ucfirst($dir).'/Views/'.$file; // 指定模块时调用模块下的文件
            }
            $error = $this->_dir.$file;
        } elseif (IS_MEMBER || $dir == 'member') {
            // 会员操作时，需要加载风格目录，如果文件不存在可以尝试调用主项目模板
            if ($dir === '/' && is_file($this->_root.$file)) {
                return $this->_root.$file;
            } elseif (is_file($this->_dir.$file)) {
                // 调用当前的会员模块目录
                return $this->_dir.$file;
            } elseif (is_file($this->_mroot.$file)) {
                // 调用默认的会员模块目录
                return $this->_mroot.$file;
            } elseif (is_file($this->_root.$file)) {
                // 调用网站主站模块目录
                return $this->_root.$file;
            }
            $error = $dir === '/' ? $this->_root.$file : $this->_dir.$file;
        } elseif ($file == 'go') {
            // 转向字段模板
            return $this->_aroot.'go.html';
        } else {
            if ($dir === '/' && is_file($this->_root.$file)) {
                // 强制主目录
                return $this->_root.$file;
            } else if (@is_file($this->_dir.$file)) {
                // 调用本目录
                return $this->_dir.$file;
            } else if (@is_file($this->_root.$file)) {
                // 再次调用主程序下的文件
                return $this->_root.$file;
            }
            $error = $dir === '/' ? $this->_root.$file : $this->_dir.$file;
        }

        // 如果移动端模板不存在就调用主网站风格
        if (IS_MOBILE && is_file(str_replace('/mobile/', '/pc/', $error))) {
            return str_replace('/mobile/', '/pc/', $error);
        } elseif (IS_MOBILE && is_file(str_replace('/mobile/', '/pc/', $this->_root.$file))) {
            return str_replace('/mobile/', '/pc/', $this->_root.$file);
        } elseif ($file == 'msg.html' && is_file(TPLPATH.'pc/default/home/msg.html')) {
            return TPLPATH.'pc/default/home/msg.html';
        }

        exit('模板文件 ('.str_replace(TPLPATH, '/', $error).') 不存在');
    }


    /**
     * 设置模板变量
     */
    public function assign($key, $value = '') {

        if (!$key) {
            return FALSE;
        }

        if (is_array($key)) {
            foreach ($key as $k => $v) {
                $this->_options[$k] = $v;
            }
        } else {
            $this->_options[$key] = $value;
        }
    }

    /**
     * 获取模板变量
     */
    public function get_value($key) {

        if (!$key) {
            return '';
        }

        return $this->_options[$key];
    }

    /**
     * 重新赋值模板变量
     */
    public function set_value($key, $value = '', $replace = '') {

        if (!$key) {
            return '';
        }

        $this->_options[$key] = $replace ? str_replace($replace, $value, $this->_options[$key]) : $value;
    }

    /**
     * 模板标签include/template
     *
     * @param	string	$name	模板文件
     * @param	string	$dir	应用、模块目录
     * @return  bool
     */
    public function _include($name, $dir = '') {

        $dir = $dir == 'MOD_DIR' ? MOD_DIR : $dir;
        $file = $this->get_file_name($name, $dir, TRUE);

        $fname = md5($file);
        isset($this->_include_file[$fname]) ? $this->_include_file[$fname] ++ : $this->_include_file[$fname] = 0;

        $this->_include_file[$fname] > 50 && exit('模板文件 ('.str_replace(TPLPATH, '/', $file).') 标签template引用文件目录结构错误');

        return $this->load_view_file($file);
    }

    /**
     * 模板标签load
     *
     * @param	string	$file	模板文件
     * @return  bool
     */
    public function _load($file) {

        $fname = md5($file);
        $this->_include_file[$fname] ++;

        $this->_include_file[$fname] > 50 && exit('模板文件 ('.str_replace(TPLPATH, '/', $file).') 标签load引用文件目录结构错误');

        return $this->load_view_file($file);
    }

    /**
     * 加载
     *
     * @param	string
     * @return  string
     */
    public function load_view_file($name) {

        $cache_file = $this->_cache.str_replace(array(WEBPATH, '/', '\\', DIRECTORY_SEPARATOR), array('', '_', '_', '_'), $name).(IS_MOBILE ? '.mobile.' : '').'.cache.php';

        // 当缓存文件不存在时或者缓存文件创建时间少于了模板文件时,再重新生成缓存文件
        if (!is_file($cache_file) || (is_file($cache_file) && is_file($name) && filemtime($cache_file) < filemtime($name))) {
            $content = $this->handle_view_file(file_get_contents($name));
            @file_put_contents($cache_file, $content, LOCK_EX) === FALSE && show_error('请将模板缓存目录（/cache/template/）权限设为777', 404, '无写入权限');
        }

        return $cache_file;
    }

    // 将模板代码转化为php
    public function code2php($code, $time = 0, $include = 1) {

        $file = md5($code).$time.'.code.php';
        !$include && $code = preg_replace([
            '#{template.*}#Uis',
            '#{load.*}#Uis'
        ], [
            '--',
            '--',
        ], $code);
        !is_file($this->_cache.$file) && @file_put_contents($this->_cache.$file, str_replace('$this->', '\Phpcmf\Service::V()->', $this->handle_view_file($code)));

        return $this->_cache.$file;
    }

    /**
     * 解析模板文件
     *
     * @param	string
     * @param	string
     * @return  string
     */
    public function handle_view_file($view_content) {

        if (!$view_content) {
            return '';
        }

        // 正则表达式匹配的模板标签
        $regex_array = [
            // 3维数组变量
            '#{\$(\w+?)\.(\w+?)\.(\w+?)\.(\w+?)}#i',
            // 2维数组变量
            '#{\$(\w+?)\.(\w+?)\.(\w+?)}#i',
            // 1维数组变量
            '#{\$(\w+?)\.(\w+?)}#i',
            // 3维数组变量
            '#\$(\w+?)\.(\w+?)\.(\w+?)\.(\w+?)#Ui',
            // 2维数组变量
            '#\$(\w+?)\.(\w+?)\.(\w+?)#Ui',
            // 1维数组变量
            '#\$(\w+?)\.(\w+?)#Ui',
            // PHP函数
            '#{([a-z_0-9]+)\((.*)\)}#Ui',
            // PHP常量
            '#{([A-Z_]+)}#',
            // PHP变量
            '#{\$(.+?)}#i',
            // 类库函数
            '#{([A-Za-z_]+)::(.+)\((.*)\)}#Ui',
            // 引入模板
            '#{\s*template\s+"([\$\-_\/\w\.]+)",\s*"(.+)"\s*}#Uis',
            '#{\s*template\s+"([\$\-_\/\w\.]+)",\s*MOD_DIR\s*}#Uis',
            '#{\s*template\s+"([\$\-_\/\w\.]+)"\s*}#Uis',
            '#{\s*template\s+([\$\-_\/\w\.]+)\s*}#Uis',
            // 加载指定文件到模板
            '#{\s*load\s+"([\$\-_\/\w\.]+)"\s*}#Uis',
            '#{\s*load\s+([\$\-_\/\w\.]+)\s*}#Uis',
            // php标签
            '#{php\s+(.+?)}#is',
            // list标签
            '#{list\s+(.+?)return=(.+?)\s?}#i',
            '#{list\s+(.+?)\s?}#i',
            '#{\s?\/list\s?}#i',
            // if判断语句
            '#{\s?if\s+(.+?)\s?}#i',
            '#{\s?else\sif\s+(.+?)\s?}#i',
            '#{\s?elseif\s+(.+?)\s?}#i',
            '#{\s?else\s?}#i',
            '#{\s?\/if\s?}#i',
            // 循环语句
            '#{\s?loop\s+\$(.+?)\s+\$(\w+?)\s?\$(\w+?)\s?}#i',
            '#{\s?loop\s+\$(.+?)\s+\$(\w+?)\s?}#i',
            '#{\s?loop\s+\$(.+?)\s+\$(\w+?)\s?=>\s?\$(\w+?)\s?}#i',
            '#{\s?\/loop\s?}#i',
            // for
            '#{for\s+(.+?)\s+(.+?)\s+(.+?)\s?}#i',
            '#{\s?\/for\s?}#i',
            // 结束标记
            '#{\s?php\s?}#i',
            '#{\s?\/php\s?}#i',
            '#\?\>\s*\<\?php\s#s',
        ];

        // 替换直接变量输出
        $replace_array = [
            "<?php echo \$\\1['\\2']['\\3']['\\4']; ?>",
            "<?php echo \$\\1['\\2']['\\3']; ?>",
            "<?php echo \$\\1['\\2']; ?>",
            "\$\\1['\\2']['\\3']['\\4']",
            "\$\\1['\\2']['\\3']",
            "\$\\1['\\2']",
            "<?php echo \\1(\\2); ?>",
            "<?php echo \\1; ?>",
            "<?php echo \$\\1; ?>",
            "<?php echo \\Phpcmf\\Service::L('\\1')->\\2(\\3); ?>",
            //"<?php echo \$this->library_method(\"\\1\",\"\\2\", \$this->_get_method(\\3));,
            "<?php if (\$fn_include = \$this->_include(\"\\1\", \"\\2\")) include(\$fn_include); ?>",
            "<?php if (\$fn_include = \$this->_include(\"\\1\", \"MOD_DIR\")) include(\$fn_include); ?>",
            "<?php if (\$fn_include = \$this->_include(\"\\1\")) include(\$fn_include); ?>",
            "<?php if (\$fn_include = \$this->_include(\"\\1\")) include(\$fn_include); ?>",
            "<?php if (\$fn_include = \$this->_load(\"\\1\")) include(\$fn_include); ?>",
            "<?php if (\$fn_include = \$this->_load(\"\\1\")) include(\$fn_include); ?>",
            "<?php \\1 ?>",
            "<?php \$list_return_\\2 = \$this->list_tag(\"\\1 return=\\2\"); if (\$list_return_\\2) { extract(\$list_return_\\2); \$count_\\2=count(\$return_\\2);} if (is_array(\$return_\\2)) { foreach (\$return_\\2 as \$key_\\2=>\$\\2) { ?>",
            "<?php \$list_return = \$this->list_tag(\"\\1\"); if (\$list_return) { extract(\$list_return); \$count=count(\$return);} if (is_array(\$return)) { foreach (\$return as \$key=>\$t) { ?>",
            "<?php } } ?>",
            "<?php if (\\1) { ?>",
            "<?php } else if (\\1) { ?>",
            "<?php } else if (\\1) { ?>",
            "<?php } else { ?>",
            "<?php } ?>",
            "<?php if (is_array(\$\\1)) { \$count=count(\$\\1);foreach (\$\\1 as \$\\2=>\$\\3) { ?>",
            "<?php if (is_array(\$\\1)) { \$count=count(\$\\1);foreach (\$\\1 as \$\\2) { ?>",
            "<?php if (is_array(\$\\1)) { \$count=count(\$\\1);foreach (\$\\1 as \$\\2=>\$\\3) { ?>",
            "<?php } } ?>",
            "<?php for (\\1 ; \\2 ; \\3) { ?>",
            "<?php }  ?>",
            "<?php ",
            " ?>",
            " ",
        ];

        // list标签别名
        foreach ($this->action as $name) {
            // 正则表达式匹配的模板标签
            $regex_array[] = '#{'.$name.'\s+(.+?)return=(.+?)\s?}#i';
            $regex_array[] = '#{'.$name.'\s+(.+?)\s?}#i';
            $regex_array[] = '#{\s?\/'.$name.'\s?}#i';
            // 替换直接变量输出
            $replace_array[] = "<?php \$list_return_\\2 = \$this->list_tag(\"action=".$name." \\1 return=\\2\"); if (\$list_return_\\2) extract(\$list_return_\\2, EXTR_OVERWRITE); \$count_\\2=count(\$return_\\2); if (is_array(\$return_\\2)) { foreach (\$return_\\2 as \$key_\\2=>\$\\2) { ?>";
            $replace_array[] = "<?php \$list_return = \$this->list_tag(\"action=".$name." \\1\"); if (\$list_return) extract(\$list_return, EXTR_OVERWRITE); \$count=count(\$return); if (is_array(\$return)) { foreach (\$return as \$key=>\$t) { ?>";
            $replace_array[] = "<?php } } ?>";
        }


        $view_content = preg_replace($regex_array, $replace_array, $view_content);

        $view_content = preg_replace_callback("/_get_var\('(.*)'\)/Ui", function ($match) {
            return "_get_var('".preg_replace('#\[\'(\w+)\'\]#Ui', '.\\1', $match[1])."')";
        }, $view_content);

        $view_content = preg_replace_callback("/list_tag\(\"(.*)\"\)/Ui", function ($match) {
            return "list_tag(\"".preg_replace('#\[\'(\w+)\'\]#Ui', '[\\1]', $match[1])."\")";
        }, $view_content);

        // 替换$ci 放最后
        $view_content = preg_replace('#\$ci\->#i', "\\Phpcmf\\Service::C()->", $view_content);

        return $view_content;
    }

    // 方法类解析
    public function library_method($class, $method, $param) {

        return call_user_func_array(
            [\Phpcmf\Service::L($class), $method],
            $param
        );
    }

    // 替换方法变量
    public function _get_method(...$params) {

        if (!$params) {
            return [];
        }

        $value = [];
        foreach ($params as $var) {
            if (strpos($var, '$') === 0) {
                $value[] = preg_replace('/\[(.+)\]/U', '[\'\\1\']', $var);
            } else {
                $value[] = $var;
            }
        }

        return $value;
    }


    // list 标签解析
    public function list_tag($_params) {

        $system = [
            'oot' => '', // 过期商品
            'num' => '', // 显示数量
            'form' => '', // 表单
            'page' => '', // 是否分页
            'site' => '', // 站点id
            'flag' => '', // 推荐位id
            'more' => '', // 是否显示栏目附加表
            'catid' => '', // 栏目id，支持多id
            'field' => '', // 显示字段
            'order' => '', // 排序
            'space' => '', // 空间uid
            'table' => '', // 表名变量
            'total' => '', // 分页总数据
            'join' => '', // 关联表名
            'on' => '', // 关联表条件
            'cache' => SYS_CACHE ? (int)SYS_CACHE_LIST * 3600 : 0, // 默认缓存时间
            'action' => '', // 动作标识
            'return' => '', // 返回变量
            'sbpage' => '', // 不按默认分页
            'module' => MOD_DIR, // 模块名称
            'modelid' => '', // 模型id
            'urlrule' => '', // 自定义分页规则
            'pagesize' => '', // 自定义分页数量
        ];

        $param = $where = [];

        // 过滤掉自定义where语句
        if (preg_match('/where=\'(.+)\'/sU', $_params, $match)) {
            $param['where'] = $match[1];
            $_params = str_replace($match[0], '', $_params);
        }

        $params = explode(' ', $_params);
        in_array($params[0], $this->action) &&  $params[0] = 'action='.$params[0];

        $sysadj = array('IN', 'BEWTEEN', 'BETWEEN', 'LIKE', 'NOTIN', 'NOT', 'BW');
        foreach ($params as $t) {
            $var = substr($t, 0, strpos($t, '='));
            $val = substr($t, strpos($t, '=') + 1);
            if (!$var) {
                continue;
            }
            $val = defined($val) ? constant($val) : $val;
            if ($var == 'fid' && !$val) {
                continue;
            }
            if (isset($system[$var])) { // 系统参数，只能出现一次，不能添加修饰符
                $system[$var] = $val;
            } else {
                if (preg_match('/^([A-Z_]+)(.+)/', $var, $match)) { // 筛选修饰符参数
                    $_pre = explode('_', $match[1]);
                    $_adj = '';
                    foreach ($_pre as $p) {
                        in_array($p, $sysadj) && $_adj = $p;
                    }
                    $where[$match[2]] = array(
                        'adj' => $_adj,
                        'name' => $match[2],
                        'value' => $val
                    );
                } else {
                    $where[$var] = array(
                        'adj' => '',
                        'name' => $var,
                        'value' => $val
                    );
                }
                $param[$var] = $val; // 用于特殊action
            }
        }

        // 替换order中的非法字符
        isset($system['order']) && $system['order'] && $system['order'] = str_ireplace(
            array('"', "'", ')', '(', ';', 'select', 'insert'),
            '',
            $system['order']
        );

        $action = $system['action'];
        // 当hits动作时，定位到moule动作
        $system['action'] == 'hits' && $system['action'] = 'module';
        // 默认站点参数
        $system['site'] = !$system['site'] ? SITE_ID : $system['site'];
        // 默认模块参数
        $system['module'] = $dirname = $system['module'] ? $system['module'] : MOD_DIR;

        // action
        switch ($system['action']) {

            case 'cache': // 系统缓存数据

                if (!isset($param['name'])) {
                    return $this->_return($system['return'], 'name参数不存在');
                }

                $pos = strpos($param['name'], '.');
                if ($pos !== FALSE) {
                    $_name = substr($param['name'], 0, $pos);
                    $_param = substr($param['name'], $pos + 1);
                } else {
                    $_name = $param['name'];
                    $_param = NULL;
                }

                $cache = $this->_cache_var($_name, $system['site']);
                if (!$cache) {
                    return $this->_return($system['return'], "缓存({$_name})不存在，请在后台更新缓存");
                } elseif ($_name == 'module-content' && $system['more']) {
                    // 读取内容模块更多信息
                    foreach ($cache as $i => $t) {
                        $cache[$i] = \Phpcmf\Service::L('cache')->get('module-'.$system['site'].'-'.$t['dirname']);
                    }
                }

                if ($_param) {
                    $data = [];
                    @eval('$data=$cache'.$this->_get_var($_param).';');
                    if (!$data) {
                        return $this->_return($system['return'], "缓存({$_name})参数不存在!!");
                    }
                } else {
                    $data = $cache;
                }

                return $this->_return($system['return'], $data, '');
                break;

            case 'category': // 栏目

                if (!$dirname) {
                    return $this->_return($system['return'], 'module参数不能为空');
                }
                $module = \Phpcmf\Service::L('cache')->get('module-'.$system['site'].'-'.$dirname);
                if (!$module) {
                    return $this->_return($system['return'], "模块({$dirname})尚未安装");
                } elseif (!$module['category']) {
                    return $this->_return($system['return'], "模块({$dirname})没有栏目数据");
                }

                $i = 0;
                $show = isset($param['show']) ? 1 : 0; // 有show参数表示显示隐藏栏目
                $return = array();
                foreach ($module['category'] as $t) {
                    if ($system['num'] && $i >= $system['num']) {
                        break;
                    } elseif (!$t['show'] && !$show) {
                        continue;
                    } elseif (isset($param['pid']) && $t['pid'] != (int)$param['pid']) {
                        continue;
                    } elseif (isset($param['mid']) && $t['mid'] != $param['mid']) {
                        continue;
                    } elseif (isset($param['child']) && $t['child'] != (int)$param['child']) {
                        continue;
                    } elseif (isset($param['id']) && !in_array($t['id'], explode(',', $param['id']))) {
                        continue;
                    } elseif (isset($system['more']) && !$system['more']) {
                        unset($t['field'], $t['setting']);
                    }
                    $return[] = $t;
                    $i ++;
                }

                if (!$return) {
                    return $this->_return($system['return'], '没有匹配到内容');
                }

                return $this->_return($system['return'], $return, '');
                break;

            case 'linkage': // 联动菜单

                $linkage = \Phpcmf\Service::L('cache')->get('linkage-'.$system['site'].'-'.$param['code']);
                if (!$linkage) {
                    return $this->_return($system['return'], "联动菜单{$param['code']}不存在，请在后台更新缓存");
                }

                // 通过别名找id
                $ids = @array_flip(\Phpcmf\Service::C()->get_cache('linkage-'.$system['site'].'-'.$param['code'].'-id'));
                if (isset($param['pid'])) {
                    if (is_numeric($param['pid'])) {
                        $pid = intval($param['pid']);
                    } elseif (!$param['pid']) {
                        $pid = 0;
                    } else {
                        $pid = isset($ids[$param['pid']]) ? $ids[$param['pid']] : 0;
                        !$pid && is_numeric($param['pid']) && \Phpcmf\Service::C()->get_cache('linkage-'.$system['site'].'-'.$param['code'].'-id', $param['pid']) && $pid = intval($param['pid']);
                    }
                }

                $i = 0;
                $return = array();
                foreach ($linkage as $t) {
                    if ($system['num'] && $i >= $system['num']) {
                        break;
                    } elseif (isset($param['pid']) && $t['pid'] != $pid) {
                        continue;
                    } elseif (isset($param['id']) && !in_array($t['id'], explode(',', $param['id']))) {
                        continue;
                    }
                    $return[] = $t;
                    $i ++;
                }

                if (!$return && isset($param['pid'])) {
                    $rpid = isset($param['fid']) ? (int)$ids[$param['fid']] : (int)$linkage[$param['pid']]['pid'];
                    foreach ($linkage as $t) {
                        if ($t['pid'] == $rpid) {
                            if ($system['num'] && $i >= $system['num']) {
                                break;
                            }
                            if (isset($param['id']) && !in_array($t['id'], explode(',', $param['id']))) {
                                continue;
                            }
                            $return[] = $t;
                            $i ++;
                        }
                    }
                    if (!$return) {
                        return $this->_return($system['return'], '没有匹配到内容');
                    }
                }

                return $this->_return($system['return'], isset($param['call']) && $param['call'] ? @array_reverse($return) : $return, '');
                break;

            case 'category_search_field': // 栏目搜索字段筛选

                $catid = $system['catid'];
                $module = \Phpcmf\Service::L('cache')->get('module-'.$system['site'].'-'.$dirname);
                if (!$module) {
                    return $this->_return($system['return'], "模块({$dirname})未安装");
                } elseif (count($module['category'][$catid]['field']) == 0) {
                    return $this->_return($system['return'], '模块未安装或者此栏目无附加字段');
                }

                $return = [];
                foreach ($module['category'][$catid]['field'] as $t) {
                    $data = dr_format_option_array($t['setting']['option']['options']);
                    if ($t['issearch'] && $t['ismain'] && in_array($t['fieldtype'], ['Select', 'Radio']) && $data) {
                        $list = [];
                        foreach ($data as $value => $name) {
                            $name && !is_null($value) && $list[] = array(
                                'name' => trim($name),
                                'value' => trim($value)
                            );
                        }
                        $list && $return[] = array(
                            'name' => $t['name'],
                            'field' => $t['fieldname'],
                            'data' => $list,
                        );
                    }
                }

                return $this->_return($system['return'], $return, '');
                break;

            case 'navigator': // 网站导航

                $navigator = \Phpcmf\Service::C()->get_cache('navigator-'.$system['site']); // 导航缓存
                if (!$navigator) {
                    return $this->_return($system['return'], '导航数据为空');
                }

                $i = 0;
                $show = isset($param['show']) ? 1 : 0; // 有show参数表示显示隐藏栏目
                $data = $navigator[(int)$param['type']];
                if (!$data) {
                    // 没有查询到内容
                    return $this->_return($system['return'], '没有查询到内容');
                }

                $field = \Phpcmf\Service::L('cache')->get('navigator-'.$system['site'].'-field');
                $return = [];
                foreach ($data as $t) {
                    if ($system['num'] && $i >= $system['num']) {
                        break;
                    } elseif (isset($param['pid']) && $t['pid'] != (int)$param['pid']) {
                        continue;
                    } elseif (isset($param['id']) && $t['id'] != (int)$param['id']) {
                        continue;
                    } elseif (!$t['show'] && !$show) {
                        continue;
                    }
                    $return[] = \Phpcmf\Service::L('Field')->format_value($field, $t, 1);
                    $i ++;
                }

                if (!$return) {
                    return $this->_return($system['return'], '没有匹配到内容');
                }

                return $this->_return($system['return'], $return, '');
                break;

            case 'page': // 自定义页调用


                $data = \Phpcmf\Service::C()->get_cache('page-'.$system['site'], 'data'); // 单页缓存
                if (!$data) {
                    return $this->_return($system['return'], '没有查询到内容');
                }

                $i = 0;
                $show = isset($param['show']) ? 1 : 0; // 有show参数表示显示隐藏栏目
                $field = \Phpcmf\Service::L('cache')->get('page-'.$system['site'].'-field');
                $return = array();
                foreach ($data as $id => $t) {
                    if (!is_numeric($id)) {
                        continue;
                    } elseif ($system['num'] && $i >= $system['num']) {
                        break;
                    } elseif (!$t['show'] && !$show) {
                        continue;
                    } elseif (isset($param['pid']) && $t['pid'] != (int) $param['pid']) {
                        continue;
                    } elseif (isset($param['id']) && !in_array($t['id'], explode(',', $param['id']))) {
                        continue;
                    }
                    $t['setting'] = dr_string2array($t['setting']);
                    $return[] = \Phpcmf\Service::L('Field')->format_value($field, $t, 1);
                    $i ++;
                }

                if (!$return) {
                    return $this->_return($system['return'], '没有匹配到内容');
                }

                return $this->_return($system['return'], $return, '');
                break;

            case 'tag': // 调用tag

                $system['order'] = $system['order'] ? ($system['order'] == 'rand' ? 'RAND()' : $system['order']) : 'displayorder asc';

                $table = \Phpcmf\Service::M()->dbprefix($system['site'].'_tag'); // tag表
                $where = '';
                if ($param['tag']) {
                    $in = $tag = $sql = [];
                    $array = explode(',', $param['tag']);
                    foreach ($array as $name) {
                        $name && $sql[] = '`name`="'.\Phpcmf\Service::M()->db->escapeString($name, true).'"';
                    }
                    $sql && $tag = $this->_query("SELECT code FROM {$table} WHERE ".implode(' OR ', $sql), $system['site'], $system['cache']);
                    if ($tag) {
                        $cache = \Phpcmf\Service::C()->get_cache('tag-'.$system['site']); // tag缓存
                        foreach ($tag as $t) {
                            if ($cache[$t['code']]['childids']) {
                                foreach ($cache[$t['code']]['childids'] as $i) {
                                    $in[] = $i;
                                }
                            }
                        }
                    }
                    $in && $where = 'WHERE id IN ('.implode(',', $in).')';
                }

                $sql = "SELECT * FROM {$table} {$where} ORDER BY ".$system['order']." LIMIT ".($system['num'] ? $system['num'] : 10);
                $data = $this->_query($sql, $system['site'], $system['cache']);

                // 没有查询到内容
                if (!$data) {
                    return $this->_return($system['return'], '没有查询到内容', $sql);
                }

                // 缓存查询结果
                $name = 'list-action-tag-'.md5($sql);
                $cache = \Phpcmf\Service::L('cache')->get_data($name);

                !$cache && $cache = $system['cache'] ? \Phpcmf\Service::L('cache')->set_data($name, $data, $system['cache']) : $data;

                return $this->_return($system['return'], $cache, $sql);
                break;

            case 'sql': // 直接sql查询

                if (preg_match('/sql=\'(.+)\'/sU', $_params, $sql)) {

                    // 替换前缀
                    $sql = str_replace(
                        array('@#S', '@#'),
                        array(\Phpcmf\Service::M()->dbprefix.$system['site'], \Phpcmf\Service::M()->dbprefix),
                        trim($sql[1])
                    );
                    stripos($sql, 'SELECT+') === 0 && $sql = urldecode($sql);
                    if (stripos($sql, 'SELECT') !== 0) {
                        return $this->_return($system['return'], 'SQL语句只能是SELECT查询语句');
                    }

                    $total = 0;
                    $pages = '';


                    // 如存在分页条件才进行分页查询
                    if ($system['page'] && $system['urlrule']) {
                        $page = max(1, (int)$_GET['page']);
                        $row = $this->_query(preg_replace('/select .* from/iUs', 'SELECT count(*) as c FROM', $sql), $system['site'], $system['cache'], FALSE);
                        $total = (int)$row['c'];
                        $pagesize = $system['pagesize'] ? $system['pagesize'] : 10;
                        // 没有数据时返回空
                        if (!$total) {
                            return $this->_return($system['return'], '没有查询到内容', $sql, 0);
                        }
                        $sql.= ' LIMIT '.$pagesize * ($page - 1).','.$pagesize;
                        $pages = $this->_get_pagination(str_replace('[page]', '{page}', urldecode($system['urlrule'])), $pagesize, $total);
                    }

                    $data = $this->_query($sql, $system['site'], $system['cache']);

                    return $this->_return($system['return'], $data, $sql, $total, $pages, $pagesize);
                } else {
                    return $this->_return($system['return'], '参数不正确，SQL语句必须用单引号包起来'); // 没有查询到内容
                }
                break;

            case 'table': // 表名查询

                if (!$system['table']) {
                    return $this->_return($system['return'], 'table参数不存在');
                }

                $tableinfo = \Phpcmf\Service::L('cache')->get_data('table-'.$system['table']);
                if (!$tableinfo) {
                    $tableinfo = \Phpcmf\Service::M('Table')->get_field($system['table']);
                    \Phpcmf\Service::L('cache')->set_data('table-'.$system['table'], $tableinfo, 36000);
                }
                if (!$tableinfo) {
                    return $this->_return($system['return'], '表('.$system['table'].')结构不存在');
                }

                $table = \Phpcmf\Service::M()->dbprefix($system['table']);
                $where = $this->_set_where_field_prefix($where, $tableinfo, $table); // 给条件字段加上表前缀
                $system['field'] = $this->_set_select_field_prefix($system['field'], $tableinfo, $table); // 给显示字段加上表前缀
                $system['order'] = $this->_set_order_field_prefix($system['order'], $tableinfo, $table); // 给排序字段加上表前缀

                $total = 0;
                $sql_from = $table; // sql的from子句

                // 关联表
                if ($system['join'] && $system['on']) {
                    $table2 = \Phpcmf\Service::M()->dbprefix($system['join']); // 关联表
                    $tableinfo2 = \Phpcmf\Service::L('cache')->get_data('table-'.$system['join']);
                    if (!$tableinfo2) {
                        \Phpcmf\Service::M('Table')->get_field($system['join']);
                        \Phpcmf\Service::L('cache')->set_data('table-'.$system['join'], $tableinfo, 36000);
                    }
                    if (!$tableinfo2) {
                        return $this->_return($system['return'], '关联数据表('.$system['join'].')结构不存在');
                    }
                    list($a, $b) = explode(',', $system['on']);
                    $b = $b ? $b : $a;
                    $sql_from.= ' LEFT JOIN '.$table2.' ON `'.$table.'`.`'.$a.'`=`'.$table2.'`.`'.$b.'`';
                }

                $sql_limit = $pages = '';
                $sql_where = $this->_get_where($where); // sql的where子句

                if ($system['page'] && $system['urlrule']) {
                    $page = max(1, (int)$_GET['page']);
                    $urlrule = str_replace('[page]', '{page}', urldecode($system['urlrule']));
                    $pagesize = (int) $system['pagesize'];
                    $pagesize = $pagesize ? $pagesize : 10;
                    $sql = "SELECT count(*) as c FROM $sql_from ".($sql_where ? "WHERE $sql_where" : "")." ORDER BY NULL";
                    $row = $this->_query($sql, $system['site'], $system['cache'], FALSE);
                    $total = (int)$row['c'];
                    // 没有数据时返回空
                    if (!$total) {
                        return $this->_return($system['return'], '没有查询到内容', $sql, 0);
                    }
                    $sql_limit = 'LIMIT '.$pagesize * ($page - 1).','.$pagesize;
                    $pages = $this->_get_pagination($urlrule, $pagesize, $total);
                } elseif ($system['num']) {
                    $sql_limit = "LIMIT {$system['num']}";
                }

                $sql = "SELECT ".($system['field'] ? $system['field'] : "*")." FROM $sql_from ".($sql_where ? "WHERE $sql_where" : "")." ".($system['order'] ? "ORDER BY {$system['order']}" : "")." $sql_limit";
                $data = $this->_query($sql, $system['site'], $system['cache']);

                // 缓存查询结果
                $name = 'list-action-table-'.md5($sql);
                $cache = \Phpcmf\Service::L('cache')->get_data($name);
                if (!$cache && is_array($data)) {
                    $cache = $system['cache'] ? \Phpcmf\Service::L('cache')->set_data($name, $data, $system['cache']) : $data;
                }

                return $this->_return($system['return'], $cache, $sql, $total, $pages, $pagesize);
                break;


            case 'form': // 表单调用

                $mid = $system['form'];
                // 表单参数为数字时按id读取
                $form = \Phpcmf\Service::C()->get_cache('form-'.$system['site'], $mid);
                // 判断是否存在
                if (!$form) {
                    return $this->_return($system['return'], "表单($mid)不存在"); // 参数判断
                }

                // 表结构缓存
                $tableinfo = \Phpcmf\Service::L('cache')->get('table-'.$system['site']);
                if (!$tableinfo) {
                    // 没有表结构缓存时返回空
                    return $this->_return($system['return'], '表结构缓存不存在');
                }
                $table = \Phpcmf\Service::M()->dbprefix($system['site'].'_form_'.$form['table']); // 主表
                if (!isset($tableinfo[$table])) {
                    return $this->_return($system['return'], '表（'.$table.'）结构缓存不存在');
                }

                // 默认条件
                $where[] = array(
                    'adj' => '',
                    'name' => 'status',
                    'value' => 1
                );

                // 将catid作为普通字段
                if (isset($system['catid']) && $system['catid']) {
                    $where[] = array(
                        'adj' => '',
                        'name' => 'catid',
                        'value' => $system['catid']
                    );
                }

                $fields = $form['field'];
                $system['order'] = !$system['order'] ? 'inputtime_desc' : $system['order']; // 默认排序参数
                $where = $this->_set_where_field_prefix($where, $tableinfo[$table], $table, $fields); // 给条件字段加上表前缀
                $system['field'] = $this->_set_select_field_prefix($system['field'], $tableinfo[$table], $table); // 给显示字段加上表前缀
                $system['order'] = $this->_set_order_field_prefix($system['order'], $tableinfo[$table], $table); // 给排序字段加上表前缀

                $total = 0;
                $sql_from = $table; // sql的from子句
                $sql_limit = $pages = '';
                $sql_where = $this->_get_where($where); // sql的where子句

                if ($system['page'] && $system['urlrule']) {
                    $page = max(1, (int)$_GET['page']);
                    $urlrule = str_replace('[page]', '{page}', urldecode($system['urlrule']));
                    $pagesize = (int) $system['pagesize'];
                    $pagesize = $pagesize ? $pagesize : 10;
                    $sql = "SELECT count(*) as c FROM $sql_from ".($sql_where ? "WHERE $sql_where" : "")." ORDER BY NULL";
                    $row = $this->_query($sql, 0, $system['cache'], FALSE);
                    $total = (int)$row['c'];
                    // 没有数据时返回空
                    if (!$total) {
                        return $this->_return($system['return'], '没有查询到内容', $sql, 0);
                    }
                    $sql_limit = 'LIMIT '.$pagesize * ($page - 1).','.$pagesize;
                    $pages = $this->_get_pagination($urlrule, $pagesize, $total);
                } elseif ($system['num']) {
                    $sql_limit = "LIMIT {$system['num']}";
                }

                $sql = "SELECT ".($system['field'] ? $system['field'] : "*")." FROM $sql_from ".($sql_where ? "WHERE $sql_where" : "")." ".($system['order'] ? "ORDER BY {$system['order']}" : "")." $sql_limit";
                $data = $this->_query($sql, 0, $system['cache']);

                // 缓存查询结果
                $name = 'list-action-form-'.md5($sql);
                $cache = \Phpcmf\Service::L('cache')->get_data($name);
                if (!$cache && is_array($data)) {
                    // 表的系统字段
                    $fields['inputtime'] = array('fieldtype' => 'Date');
                    $dfield = \Phpcmf\Service::L('Field')->app('form');
                    foreach ($data as $i => $t) {
                        $data[$i] = $dfield->format_value($fields, $t, 1, 'form');
                    }
                    $cache = $system['cache'] ? \Phpcmf\Service::L('cache')->set_data($name, $data, $system['cache']) : $data;
                }

                return $this->_return($system['return'], $cache, $sql, $total, $pages, $pagesize);
                break;

            case 'mform': // 模块表单调用

                $mid = $system['form'];
                $form = \Phpcmf\Service::L('cache')->get('module-'.$system['site'].'-'.$dirname, 'form', $mid);
                // 判断是否存在
                if (!$form) {
                    return $this->_return($system['return'], "模块{$dirname}表单({$mid})不存在"); // 参数判断
                }

                $tableinfo = \Phpcmf\Service::L('cache')->get('table-'.$system['site']);
                if (!$tableinfo) {
                    // 没有表结构缓存时返回空
                    return $this->_return($system['return'], '表结构缓存不存在');
                }

                $table = \Phpcmf\Service::M()->dbprefix($system['site'].'_'.$dirname.'_form_'.$form['table']); // 模块主表
                if (!isset($tableinfo[$table])) {
                    return $this->_return($system['return'], '表（'.$table.'）结构缓存不存在');
                }

                // 默认条件
                $where[] = array(
                    'adj' => '',
                    'name' => 'status',
                    'value' => 1
                );


                $fields = $form['fields'];
                $system['order'] = !$system['order'] ? 'inputtime_desc' : $system['order']; // 默认排序参数

                $where = $this->_set_where_field_prefix($where, $tableinfo[$table], $table, $fields); // 给条件字段加上表前缀
                $system['field'] = $this->_set_select_field_prefix($system['field'], $tableinfo[$table], $table); // 给显示字段加上表前缀
                $system['order'] = $this->_set_order_field_prefix($system['order'], $tableinfo[$table], $table); // 给排序字段加上表前缀

                $total = 0;
                $fields = $form['field']; // 主表的字段
                $sql_from = $table; // sql的from子句
                $sql_where = $this->_get_where($where); // sql的where子句
                $sql_limit = $pages = '';

                if ($system['page'] && $system['urlrule']) {
                    $page = max(1, (int)$_GET['page']);
                    $urlrule = str_replace('[page]', '{page}', urldecode($system['urlrule']));
                    $pagesize = (int) $system['pagesize'];
                    $pagesize = $pagesize ? $pagesize : 10;
                    $sql = "SELECT count(*) as c FROM $sql_from ".($sql_where ? "WHERE $sql_where" : "")." ORDER BY NULL";
                    $row = $this->_query($sql, 0, $system['cache'], FALSE);
                    $total = (int)$row['c'];
                    // 没有数据时返回空
                    if (!$total) {
                        return $this->_return($system['return'], '没有查询到内容', $sql, 0);
                    }
                    $sql_limit = 'LIMIT '.$pagesize * ($page - 1).','.$pagesize;
                    $pages = $this->_get_pagination($urlrule, $pagesize, $total);
                } elseif ($system['num']) {
                    $sql_limit = "LIMIT {$system['num']}";
                }

                $sql = "SELECT ".($system['field'] ? $system['field'] : "*")." FROM $sql_from ".($sql_where ? "WHERE $sql_where" : "")." ".($system['order'] ? "ORDER BY {$system['order']}" : "")." $sql_limit";
                $data = $this->_query($sql, 0, $system['cache']);

                // 缓存查询结果
                $name = 'list-action-mform-'.md5($sql);
                $cache = \Phpcmf\Service::L('cache')->get_data($name);
                if (!$cache && is_array($data)) {
                    // 表的系统字段
                    $fields['inputtime'] = array('fieldtype' => 'Date');
                    $dfield = \Phpcmf\Service::L('Field')->app($dirname);
                    foreach ($data as $i => $t) {
                        $data[$i] = $dfield->format_value($fields, $t, 1, 'form');
                    }
                    $cache = $system['cache'] ? \Phpcmf\Service::L('cache')->set_data($name, $data, $system['cache']) : $data;
                }

                return $this->_return($system['return'], $cache, $sql, $total, $pages, $pagesize);
                break;

            case 'member': // 会员信息

                $tableinfo = \Phpcmf\Service::L('cache')->get('table-'.$system['site']);
                if (!$tableinfo) {
                    // 没有表结构缓存时返回空
                    return $this->_return($system['return'], '表结构缓存不存在');
                }

                $table = \Phpcmf\Service::M()->dbprefix('member'); // 主表
                if (!isset($tableinfo[$table])) {
                    return $this->_return($system['return'], '表（'.$table.'）结构缓存不存在');
                }

                $fields = \Phpcmf\Service::C()->get_cache('member', 'field');

                // groupid查询
                if (isset($param['groupid']) && $param['groupid']) {

                    if (strpos($param['groupid'], ',') !== false) {
                        $gwhere = ' `'.$table.'`.`id` in (select uid from `'.$table.'_group_index` where `gid` = in ('.dr_safe_replace($param['groupid']).'))';
                    } elseif (strpos($param['groupid'], '-') !== false) {
                        $arr = explode('-', $param['groupid']);
                        $gwhere = [];
                        foreach ($arr as $t) {
                            $t = intval($t);
                            $t && $gwhere[] = ' `'.$table.'`.`id` in (select uid from `'.$table.'_group_index` where `gid` = '. $t.')';
                        }
                        $gwhere = $gwhere ? '('.implode(' AND ', $gwhere).')' : '';
                    } else {
                        $gwhere = ' `'.$table.'`.`id` in (select uid from `'.$table.'_group_index` where `gid` = '. intval($param['groupid']).')';
                    }

                    $gwhere && $where['id'] = [
                        'adj' => 'SQL',
                        'name' => 'id',
                        'value' => $gwhere
                    ];
                    unset($param['groupid']);
                }

                $system['order'] = !$system['order'] ? 'id' : $system['order']; // 默认排序参数

                $where = $this->_set_where_field_prefix($where, $tableinfo[$table], $table, $fields); // 给条件字段加上表前缀
                $system['field'] = $this->_set_select_field_prefix($system['field'], $tableinfo[$table], $table); // 给显示字段加上表前缀
                $system['order'] = $this->_set_order_field_prefix($system['order'], $tableinfo[$table], $table); // 给排序字段加上表前缀

                $sql_from = $table; // sql的from子句

                if ($system['more']) {
                    // 会员附表
                    $more = \Phpcmf\Service::M()->dbprefix('member_data'); // 附表
                    $where = $this->_set_where_field_prefix($where, $tableinfo[$more], $more, $fields); // 给条件字段加上表前缀
                    $system['field'] = $this->_set_select_field_prefix($system['field'], $tableinfo[$more], $more); // 给显示字段加上表前缀
                    $system['order'] = $this->_set_order_field_prefix($system['order'], $tableinfo[$more], $more); // 给排序字段加上表前缀
                    $sql_from.= " LEFT JOIN $more ON `$table`.`id`=`$more`.`id`"; // sql的from子句
                }

                $total = 0;
                $sql_limit = '';
                $sql_where = $this->_get_where($where); // sql的where子句

                if ($system['page'] && $system['urlrule']) { // 如存在分页条件才进行分页查询
                    $page = max(1, (int)$_GET['page']);
                    $urlrule = str_replace('[page]', '{page}', urldecode($system['urlrule']));
                    $pagesize = (int) $system['pagesize'];
                    $pagesize = $pagesize ? $pagesize : 10;
                    $sql = "SELECT count(*) as c FROM $sql_from ".($sql_where ? "WHERE $sql_where" : "")." ORDER BY NULL";
                    $row = $this->_query($sql, $system['site'], $system['cache'], FALSE);
                    $total = (int)$row['c'];
                    if (!$total) {
                        // 没有数据时返回空
                        return $this->_return($system['return'], '没有查询到内容', $sql, 0);
                    }
                    $sql_limit = ' LIMIT '.$pagesize * ($page - 1).','.$pagesize;
                    $pages = $this->_get_pagination(str_replace('[page]', '{page}', $urlrule), $pagesize, $total);
                } elseif ($system['num']) {
                    $sql_limit = "LIMIT {$system['num']}";
                }

                $sql = "SELECT ".($system['field'] ? $system['field'] : "*")." FROM $sql_from ".($sql_where ? "WHERE $sql_where" : "")." ".($system['order'] == "null" ? "" : " ORDER BY {$system['order']}")." $sql_limit";
                $data = $this->_query($sql, $system['site'], $system['cache']);

                // 缓存查询结果
                $name = 'list-action-member-'.md5($sql);
                $cache = \Phpcmf\Service::L('cache')->get_data($name);
                if (!$cache && is_array($data)) {
                    // 系统字段
                    $fields['regtime'] = array('fieldtype' => 'Date');
                    // 格式化显示自定义字段内容
                    $dfield = \Phpcmf\Service::L('Field')->app('member');
                    foreach ($data as $i => $t) {
                        $data[$i] = $dfield->format_value($fields, $t, 1, 'member');
                    }
                    $cache = $system['cache'] ? \Phpcmf\Service::L('cache')->set_data($name, $data, $system['cache']) : $data;
                }

                return $this->_return($system['return'], $cache, $sql, $total, $pages, $pagesize);
                break;


            case 'content': // 模块内容详细页面

                $module = \Phpcmf\Service::L('cache')->get('module-'.$system['site'].'-'.$dirname);
                if (!$module) {
                    return $this->_return($system['return'], "模块({$dirname})未安装");
                }

                $tableinfo = \Phpcmf\Service::L('cache')->get('table-'.$system['site']);
                if (!$tableinfo) {
                    // 没有表结构缓存时返回空
                    return $this->_return($system['return'], '表结构缓存不存在');
                }

                $table = \Phpcmf\Service::M()->dbprefix($system['site'].'_'.$module['dirname']); // 模块主表`
                if (!isset($tableinfo[$table])) {
                    return $this->_return($system['return'], '表（'.$table.'）结构缓存不存在');
                }

                // 初始化数据表
                $db = \Phpcmf\Service::M('Content', $dirname);
                $db->_init($dirname, $system['site']);
                $data = $db->get_data(intval($param['id']));

                // 缓存查询结果
                $name = 'list-action-content-'.md5($table.$param['id']);
                $cache = \Phpcmf\Service::L('cache')->get_data($name);
                if (!$cache && is_array($data)) {
                    // 模块表的系统字段
                    $fields = $module['field']; // 主表的字段
                    $fields['inputtime'] = array('fieldtype' => 'Date');
                    $fields['updatetime'] = array('fieldtype' => 'Date');
                    // 格式化显示自定义字段内容
                    $dfield = \Phpcmf\Service::L('Field')->app($module['dirname']);
                    $data = $dfield->format_value($fields, $data, 1, $module['dirname']);
                    $cache = $system['cache'] ? \Phpcmf\Service::L('cache')->set_data($name, $data, $system['cache']) : $data;
                }

                return $this->_return($system['return'], $cache ? $cache : $data);
                break;

            case 'related': // 模块的相关文章

                if (!$param['tag']) {
                    return $this->_return($system['return'], '没有查询到内容'); // 没有查询到内容
                }

                $table = \Phpcmf\Service::M()->dbprefix($system['site'].'_'.$dirname); // 模块主表

                $sql = [];
                $array = explode(',', $param['tag']);
                foreach ($array as $name) {
                    $name && $sql[] = '(`'.$table.'`.`title` LIKE "%'.\Phpcmf\Service::M()->db->escapeString($name, true).'%" OR `'.$table.'`.`keywords` LIKE "%'.\Phpcmf\Service::M()->db->escapeString($name, true).'%")';
                }
                $sql = $where[] = [
                    'ajd' => 'SQL',
                    'value' => '('.implode(' OR ', $sql).')'
                ];
                unset($param['tag']);
                // 跳转到module方法
                goto module;
                break;

            case 'search': // 模块的搜索

                $total = (int)$system['total'];
                unset($system['total']);

                // 没有数据时返回空
                if (!$total) {
                    return $this->_return($system['return'], 'total参数为空', '', 0);
                } elseif (!$dirname) {
                    return $this->_return($system['return'], 'module参数不能为空');
                } elseif (!$param['id']) {
                    return $this->_return($system['return'], 'id参数为空', '', 0);
                }

                if ($where) {
                    foreach ($where as $i => $t) {
                        if ($t['name'] == 'id') {
                            unset($where[$i]);
                        }
                    }
                }

                $table = \Phpcmf\Service::M()->dbprefix($system['site'].'_'.$dirname); // 模块主表
                $where[] = [
                    'ajd' => 'SQL',
                    'value' => '(`'.$table.'`.`id` IN(SELECT `cid` FROM `'.$table.'_search_index` WHERE `id`="'.$param['id'].'"))'
                ];
                unset($param['id']);

                $system['sbpage'] = 1;

                // 跳转到module方法
                goto module;
                break;

            case 'module': // 模块数据

                module:

                $module = \Phpcmf\Service::L('cache')->get('module-'.$system['site'].'-'.$dirname);
                if (!$module) {
                    return $this->_return($system['return'], "模块({$dirname})未安装");
                }

                $tableinfo = \Phpcmf\Service::L('cache')->get('table-'.$system['site']);
                if (!$tableinfo) {
                    // 没有表结构缓存时返回空
                    return $this->_return($system['return'], '表结构缓存不存在');
                }

                $table = \Phpcmf\Service::M()->dbprefix($system['site'].'_'.$module['dirname']); // 模块主表`

                if (!isset($tableinfo[$table])) {
                    return $this->_return($system['return'], '表（'.$table.'）结构缓存不存在');
                }

                // 是否操作自定义where
                if ($param['where']) {
                    $where[] = [
                        'ajd' => 'SQL',
                        'value' => urldecode($param['where'])
                    ];
                    unset($param['where']);
                }


                // 排序操作
                if (!$system['order'] && $where[0]['adj'] == 'IN' && $where[0]['name'] == 'id') {
                    // 按id序列来排序
                    $system['order'] = strlen($where[0]['value']) < 10000 && $where[0]['value'] ? 'instr("'.$where[0]['value'].'", `'.$table.'`.`id`)' : 'NULL';
                } else {
                    // 默认排序参数
                    !$system['order'] && ($system['order'] = $system['flag'] ? 'updatetime_desc' : ($action == 'hits' ? 'hits' : 'updatetime'));
                }

                // 栏目筛选
                if ($system['catid']) {
                    if (strpos($system['catid'], ',') !== FALSE) {
                        $temp = @explode(',', $system['catid']);
                        if ($temp) {
                            $catids = array();
                            foreach ($temp as $i) {
                                $catids = $module['category'][$i]['child'] ? array_merge($catids, $module['category'][$i]['catids']) : array_merge($catids, array($i));
                            }
                            $catids && $where[] = array(
                                'adj' => 'IN',
                                'name' => 'catid',
                                'value' => implode(',', $catids),
                            );
                            unset($catids);
                        }
                        unset($temp);
                    } elseif ($module['category'][$system['catid']]['child']) {
                        $where[] = array(
                            'adj' => 'IN',
                            'name' => 'catid',
                            'value' => $module['category'][$system['catid']]['childids']
                        );
                    } else {
                        $where[] = array(
                            'adj' => '',
                            'name' => 'catid',
                            'value' => (int)$system['catid']
                        );
                    }
                }

                $fields = $module['field']; // 主表的字段
                $where[] = array( 'adj' => '', 'name' => 'status', 'value' => 9);
                $where = $this->_set_where_field_prefix($where, $tableinfo[$table], $table, $fields); // 给条件字段加上表前缀
                $system['field'] = $this->_set_select_field_prefix($system['field'], $tableinfo[$table], $table); // 给显示字段加上表前缀
                $system['order'] = $this->_set_order_field_prefix($system['order'], $tableinfo[$table], $table); // 给排序字段加上表前缀

                // sql的from子句
                if ($action == 'hits') {
                    $sql_from = '`'.$table.'` LEFT JOIN `'.$table.'_hits` ON `'.$table.'`.`id`=`'.$table.'_hits`.`id`';
                    $table_more = $table.'_hits'; // hits表
                    $system['field'] = $this->_set_select_field_prefix($system['field'], $tableinfo[$table_more], $table_more); // 给显示字段加上表前缀
                    $system['order'] = $this->_set_order_field_prefix($system['order'], $tableinfo[$table_more], $table_more); // 给排序字段加上表前缀
                } else {
                    $sql_from = '`'.$table.'`';
                }

                // 关联栏目附加表
                if ($system['more']) {
                    $_catid = (int)$system['catid'];
                    if (isset($module['category'][$_catid]['field']) && $module['category'][$_catid]['field']) {
                        $fields = array_merge($fields, $module['category'][$_catid]['field']);
                        $table_more = $table.'_category_data'; // 栏目附加表
                        $where = $this->_set_where_field_prefix($where, $tableinfo[$table_more], $table_more, $fields); // 给条件字段加上表前缀
                        $system['field'] = $this->_set_select_field_prefix($system['field'], $tableinfo[$table_more], $table_more); // 给显示字段加上表前缀
                        $system['order'] = $this->_set_order_field_prefix($system['order'], $tableinfo[$table_more], $table_more); // 给排序字段加上表前缀
                        $sql_from.= " LEFT JOIN $table_more ON `$table_more`.`id`=`$table`.`id`"; // sql的from子句
                    }
                }

                // 关联表
                if ($system['join'] && $system['on']) {
                    $table_more = \Phpcmf\Service::M()->db->dbprefix($system['join']); // 关联表
                    if (!$tableinfo[$table_more]) {
                        return $this->_return($system['return'], '关联数据表（'.$table_more.'）不存在');
                    }
                    list($a, $b) = explode(',', $system['on']);
                    $b = $b ? $b : $a;
                    $where = $this->_set_where_field_prefix($where, $tableinfo[$table_more], $table_more); // 给条件字段加上表前缀
                    $system['field'] = $this->_set_select_field_prefix($system['field'], $tableinfo[$table_more], $table_more); // 给显示字段加上表前缀
                    $system['order'] = $this->_set_order_field_prefix($system['order'], $tableinfo[$table_more], $table_more); // 给排序字段加上表前缀
                    $sql_from.= ' LEFT JOIN `'.$table_more.'` ON `'.$table.'`.`'.$a.'`=`'.$table_more.'`.`'.$b.'`';
                }

                $sql_limit = $pages = '';
                $sql_where = $this->_get_where($where, $fields); // sql的where子句

                // 商品有效期
                // isset($fields['order_etime']) && ($system['oot'] ? $sql_where.= ' AND `order_etime` BETWEEN 1 AND '.SYS_TIME : $sql_where.= ' AND NOT (`order_etime` BETWEEN 1 AND '.SYS_TIME.')');

                // 推荐位调用
                if ($system['flag']) {
                    $_w = strpos($system['flag'], ',') ? '`flag` IN ('.$system['flag'].')' : '`flag`='.(int)$system['flag'];
                    $_i = $this->_query("select id from {$table}_flag where ".$_w.($system['num'] ? " LIMIT {$system['num']}" : ""), $system['site'], $system['cache']);
                    $in = array();
                    foreach ($_i as $t) {
                        $in[] = $t['id'];
                    }
                    // 没有查询到内容
                    if (!$in) {
                        return $this->_return($system['return'], '没有查询到推荐位内容');
                    }
                    $sql_where = ($sql_where ? $sql_where.' AND' : '')."`$table`.`id` IN (".implode(',', $in).")";
                    unset($_w, $_i, $in);
                }

                if ($system['page']) {
                    $page = max(1, (int)$_GET['page']);
                    if (!$system['sbpage'] && $system['catid'] && is_numeric($system['catid'])) {
                        $system['urlrule'] = \Phpcmf\Service::L('router')->category_url($module, $module['category'][$system['catid']], '{page}');
                        $system['pagesize'] = (int)$module['category'][$system['catid']]['setting']['template']['pagesize'];
                    }
                    $urlrule = str_replace('[page]', '{page}', urldecode($system['urlrule']));
                    $pagesize = (int)$system['pagesize'];
                    !$pagesize && $pagesize = 10;
                    if (!$total) {
                        $sql = "SELECT count(*) as c FROM $sql_from ".($sql_where ? "WHERE $sql_where" : "")." ORDER BY NULL";
                        $row = $this->_query($sql, $system['site'], $system['cache'], FALSE);
                        $total = (int)$row['c'];
                        // 没有数据时返回空
                        if (!$total) {
                            return $this->_return($system['return'], '没有查询到内容', $sql, 0);
                        }
                    }
                    $pages = $this->_get_pagination($urlrule, $pagesize, $total);
                    $sql_limit = 'LIMIT '.$pagesize * ($page - 1).','.$pagesize;
                } elseif ($system['num']) {
                    $pages = '';
                    $sql_limit = "LIMIT {$system['num']}";
                }

                $sql = "SELECT ".$this->_get_select_field($system['field'] ? $system['field'] : '*')." FROM $sql_from ".($sql_where ? "WHERE $sql_where" : "").($system['order'] == "null" ? "" : " ORDER BY {$system['order']}")." $sql_limit";
                $data = $this->_query($sql, $system['site'], $system['cache']);

                // 缓存查询结果
                $name = 'list-action-module-'.md5($sql);
                $cache = \Phpcmf\Service::L('cache')->get_data($name);
                if (!$cache && is_array($data)) {
                    // 模块表的系统字段
                    $fields['inputtime'] = array('fieldtype' => 'Date');
                    $fields['updatetime'] = array('fieldtype' => 'Date');
                    // 格式化显示自定义字段内容
                    $dfield = \Phpcmf\Service::L('Field')->app($module['dirname']);
                    foreach ($data as $i => $t) {
                        $data[$i] = $dfield->format_value($fields, $t, 1, $module['dirname']);
                    }
                    $cache = $system['cache'] ? \Phpcmf\Service::L('cache')->set_data($name, $data, $system['cache']) : $data;
                }

                return $this->_return($system['return'], $cache ? $cache : $data, $sql, $total, $pages, $pagesize);
                break;


            default :
                return $this->_return($system['return'], '无此标签参数');
                break;
        }
    }

    /**
     * 查询缓存
     */
    public function _query($sql, $site, $cache, $all = TRUE) {

        // 缓存存在时读取缓存文件
        $cname = md5($site.$sql.dr_now_url());
        if (SYS_CACHE && $cache && $data = \Phpcmf\Service::L('cache')->get_data($cname)) {
            return $data;
        }

        // 执行SQL
        $query = \Phpcmf\Service::M()->db->query($sql);

        if (!$query) {
            return 'SQL查询解析不正确：'.$sql;
        }

        // 查询结果
        $data = $all ? $query->getResultArray() : $query->getRowArray();

        // 开启缓存时，重新存储缓存数据
        $cache && \Phpcmf\Service::L('cache')->set_data($cname, $data, $cache);

        return $data;
    }

    /**
     * 分页
     */
    public function _get_pagination($url, $pagesize, $total) {

        // 这里要支持移动端分页条件
        if (IS_MOBILE && is_file(WEBPATH.'config/page_mobile.php')) {
            $config = require WEBPATH.'config/page_mobile.php';
        } else {
            $config = require WEBPATH.'config/page.php';
        }

        $config['base_url'] = $url;
        $config['per_page'] = $pagesize;
        $config['total_rows'] = $total;
        $config['use_page_numbers'] = TRUE;
        $config['query_string_segment'] = 'page';

        return \Phpcmf\Service::L('Page')->initialize($config)->create_links();
    }

    // 条件子句格式化
    public function _get_where($where) {

        if ($where) {
            $string = '';
            foreach ($where as $i => $t) {
                // 过滤字段
                if (isset($t['use']) && $t['use'] == 0 || !strlen($t['value'])) {
                    continue;
                }
                $join = $string ? ' AND' : '';
                switch ($t['adj']) {
                    case 'LIKE':
                        $string.= $join." {$t['name']} LIKE \"".dr_safe_replace($t['value'])."\"";
                        break;

                    case 'IN':
                        $string.= $join." {$t['name']} IN (".dr_safe_replace($t['value']).")";
                        break;

                    case 'NOTIN':
                        $string.= $join." {$t['name']} NOT IN (".dr_safe_replace($t['value']).")";
                        break;

                    case 'NOT':
                        $string.= $join.(is_numeric($t['value']) ? " {$t['name']} <> ".$t['value'] : " {$t['name']} <> \"".($t['value'] == "''" ? '' : dr_safe_replace($t['value']))."\"");
                        break;

                    case 'BETWEEN':
                        $string.= $join." {$t['name']} BETWEEN ".str_replace(',', ' AND ', $t['value'])."";
                        break;

                    case 'BEWTEEN':
                        $string.= $join." {$t['name']} BETWEEN ".str_replace(',', ' AND ', $t['value'])."";
                        break;

                    case 'BW':
                        $string.= $join." {$t['name']} BETWEEN ".str_replace(',', ' AND ', $t['value'])."";
                        break;

                    case 'SQL':
                        $string.= $join.' '.$t['value'];
                        break;

                    default:
                        if (strpos($t['name'], '`thumb`')) {
                            // 缩略图筛选
                            $t['value'] == 1 ? $string.= $join." {$t['name']}<>''" : $string.= $join." {$t['name']}=''";
                        } elseif (!$t['name'] && $t['value']) {

                            $string.= $join.' '.$t['value'];
                        } else {
                            $string.= $join.(is_numeric($t['value']) ? " {$t['name']} = ".$t['value'] : " {$t['name']} = \"".($t['value'] == "''" ? '' : dr_safe_replace($t['value']))."\"");
                        }
                        break;
                }
            }
            return trim($string);
        }

        return 1;
    }

    // 给条件字段加上表前缀
    public function _set_where_field_prefix($where, $field, $prefix, $myfield = []) {

        if (!$where) {
            return [];
        }

        foreach ($where as $i => $t) {
            if (in_array($t['name'], $field)) {
                $where[$i]['use'] = 1;
                $where[$i]['name'] = "`$prefix`.`{$t['name']}`";
                if ($myfield && $myfield[$t['name']]['fieldtype'] == 'Linkage') {
                    // 联动菜单
                    $data = dr_linkage($myfield[$t['name']]['setting']['option']['linkage'], $t['value']);
                    if ($data) {
                        if ($data['child']) {
                            $where[$i]['adj'] = 'IN';
                            $where[$i]['value'] = $data['childids'];
                        } else {
                            $where[$i]['value'] = intval($data['ii']);
                        }
                    }
                }
            } elseif (!$t['name'] && $t['value']) {
                // 标示只有where的条件查询
                $where[$i]['use'] = 1;
            } else {
                $where[$i]['use'] = $t['use'] ? 1 : 0;
            }
        }

        return $where;
    }

    // 给显示字段加上表前缀
    public function _set_select_field_prefix($select, $field, $prefix) {

        $select = str_replace('DISTINCT_', 'DISTINCT ', $select);

        if ($select) {
            $array = explode(',', $select);
            foreach ($array as $i => $t) {
                in_array($t, $field) && $array[$i] = "`$prefix`.`$t`";
            }
            return implode(',', $array);
        }

        return $select;
    }

    // 给排序字段加上表前缀
    public function _set_order_field_prefix($order, $field, $prefix) {

        if ($order) {
            if (in_array(strtoupper($order), ['RAND()', 'RAND'])) {
                // 随机排序
                return 'RAND()';
            } else {
                // 字段排序
                $array = explode(',', $order);
                foreach ($array as $i => $t) {
                    if (strpos($t, '`') !== false) {
                        $array[$i] = $t;
                        continue;
                    }
                    $a = explode('_', $t);
                    $b = end($a);
                    if (in_array(strtolower($b), ['desc', 'asc'])) {
                        $a = str_replace('_'.$b, '', $t);
                    } else {
                        $a = $t;
                        $b = '';
                    }
                    $b = strtoupper($b);
                    if (in_array($a, $field)) {
                        $array[$i] = "`$prefix`.`$a` ".($b ? $b : "DESC");
                    } elseif (in_array($a.'_lat', $field) && in_array($a.'_lng', $field)) {
                        if (\Phpcmf\Service::C()->my_position) {
                            $this->pos_order = $a;
                            $array[$i] = $a.' ASC';
                        } else {
                            \Phpcmf\Service::C()->msg('没有定位到您的坐标');
                        }
                    } else {
                        $array[$i] = $t;
                    }
                }
                return implode(',', $array);
            }
        }

        return NULL;
    }

    // 格式化查询参数
    private function _get_select_field($field) {

        $this->pos_order && (\Phpcmf\Service::C()->my_position ? $field.= ',ROUND(6378.138*2*ASIN(SQRT(POW(SIN(('.\Phpcmf\Service::C()->my_position['lat'].'*PI()/180-'.$this->pos_order.'_lat*PI()/180)/2),2)+COS('.\Phpcmf\Service::C()->my_position['lat'].'*PI()/180)*COS('.$this->pos_order.'_lat*PI()/180)*POW(SIN(('.\Phpcmf\Service::C()->my_position['lng'].'*PI()/180-'.$this->pos_order.'_lng*PI()/180)/2),2)))*1000) AS '.$this->pos_order : \Phpcmf\Service::C()->msg('没有定位到您的坐标'));

        return $field;
    }

    // list 返回
    public function _return($return, $data = [], $sql = '', $total = 0, $pages = '', $pagesize = 0) {

        $debug = $sql;
        if ($data && !is_array($data)) {
            $debug.= $data;
            $data = [];
        }

        $total = isset($total) ? $total : count($data);
        $page = max(1, (int)$_GET['page']);
        $nums = $pagesize ? ceil($total/$pagesize) : 0;

        // 返回数据格式
        if ($return) {
            return [
                'nums_'.$return => $nums,
                'pageid_'.$return => $page,
                'pages_'.$return => $pages,
                'debug_'.$return => $debug,
                'total_'.$return => $total,
                'return_'.$return => $data,
                'pagesize_'.$return => $pagesize,
            ];
        } else {
            return [
                'nums' => $nums,
                'debug' => $debug,
                'pageid' => $page,
                'total' => $total,
                'pages' => $pages,
                'return' => $data,
                'pagesize' => $pagesize,
            ];
        }
    }

    // 替换变量
    public function _get_var($param) {

        $array = explode('.', $param);
        if (!$array) {
            return '';
        }

        $string = '';
        foreach ($array as $var) {
            $var = dr_safe_replace($var);
            $string.= '[';
            if (strpos($var, '$') === 0) {
                $string.= preg_replace('/\[(.+)\]/U', '[\'\\1\']', $var);
            } elseif (preg_match('/[A-Z_]+/', $var)) {
                $string.= ''.$var.'';
            } else {
                $string.= '\''.$var.'\'';
            }
            $string.= ']';
        }

        return $string;
    }

    // 公共变量参数
    public function _cache_var($name, $siteid = 0) {

        $data = null;
        $name = strtolower($name);

        switch ($name) {
            case 'member':
                $data = \Phpcmf\Service::L('cache')->get('member');
                break;
            case 'urlrule':
                $data = \Phpcmf\Service::L('cache')->get('urlrule');
                break;
            case 'module-content':
                $data = \Phpcmf\Service::L('cache')->get('module-'.$siteid.'-content');
                break;
            case 'category':
                $data = \Phpcmf\Service::L('cache')->get('module-'.$siteid.'-'.MOD_DIR, 'category');
                break;
            case 'page':
                $data = \Phpcmf\Service::L('cache')->get('page-'.$siteid, 'data');
                break;
            default:
                $data = \Phpcmf\Service::L('cache')->get($name.'-'.$siteid);
                break;
        }

        return $data;
    }

    // 模板中的全部变量
    public function get_data() {
        return $this->_options;
    }

}