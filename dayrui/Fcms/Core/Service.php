<?php namespace Phpcmf;

use \Phpcmf\View;

class Service
{

    /**
     * @var array
     */
    static private $instances = [];

    /**
     * @var object
     */
    static private $view;

    /**
     * @var object
     */
    static private $model;

    /**
     * 控制器对象实例
     *
     * @var object
     */
    public static function C() {
        return class_exists('\Phpcmf\Common') ? \Phpcmf\Common::get_instance() : null;
    }

    /**
     * 模板视图对象实例
     *
     * @var object
     */
    public static function V() {

        if (!is_object(static::$view)) {
            static::$view = new \Phpcmf\View();
        }

        return static::$view;
    }

    /**
     * 模型类对象实例
     *
     * @var object
     */
    public static function model() {

        if (!is_object(static::$model)) {
            static::$model = new \Phpcmf\Model();
        }

        return static::$model;
    }

    /**
     * 类对象实例
     *
     * @var object
     */
    public static function L( $name,  $namespace = '') {

        list($classFile, $extendFile) = self::_get_class_file($name, $namespace, 'Library');

        $_cname = md5($classFile);
        $className = ucfirst($name);
        if (!isset(static::$instances[$_cname]) or !is_object(static::$instances[$_cname])) {
            require $classFile;
            // 自定义继承类
            if ($extendFile && is_file($extendFile)) {
                require $extendFile;
                $className = '\\Phpcmf\\Library\\My_'.$className;
            } else {
                $className = '\\Phpcmf\\Library\\'.$className;
            }
            static::$instances[$_cname] = new $className();
        }

        return static::$instances[$_cname];

    }

    /**
     * 模型对象实例
     *
     * @var object
     */
    public static function M( $name = '',  $namespace = '') {

        if (!$name) {
            return static::model();
        }

        list($classFile, $extendFile) = self::_get_class_file($name, $namespace, 'Model');

        $_cname = md5($classFile);
        $className = ucfirst($name);
        if (!isset(static::$instances[$_cname]) or !is_object(static::$instances[$_cname])) {
            require $classFile;
            // 自定义继承类
            if ($extendFile && is_file($extendFile)) {
                require $extendFile;
                $className = '\\Phpcmf\\Model\\My_'.$className;
            } else {
                $className = '\\Phpcmf\\Model\\'.$className;
            }
            static::$instances[$_cname] = new $className();
        }

        return static::$instances[$_cname];
    }

    // 获取类文件路径
    private static function _get_class_file($name, $namespace, $class) {

        $className = ucfirst($name);
        $classFile = CMSPATH.$class.'/'.$className.'.php';

        // 自定义继承类文件
        $extendFile = MYPATH.$class.'/'.$className.'.php';
        !is_file($extendFile) && $namespace && $extendFile = APPSPATH.ucfirst($namespace).'/'.$class.'s/'.$className.'.php';

        if (!is_file($classFile)) {
            // 相对于APP目录下的模型
            if ($namespace) {
                $classFile = APPSPATH.ucfirst($namespace).'/'.$class.'s/'.$className.'.php';
                $extendFile = '';
            } else if (is_file($extendFile)) {
                $classFile = $extendFile;
                $extendFile = '';
            }
            (!$classFile || !is_file($classFile)) && (defined('IS_API_AUTH_POST') && IS_API_AUTH_POST ? \Phpcmf\Common::json(0, '类文件：'.str_replace(FCPATH, '', $classFile).'不存在') : exit('类文件：'.str_replace(FCPATH, '', $classFile).'不存在'));
        }

        return [$classFile, $extendFile];
    }
}