<?php namespace Phpcmf\Field;

class Diy extends \Phpcmf\Library\A_Field {
	
	/**
     * 构造函数
     */
    public function __construct(...$params) {
        parent::__construct(...$params);
		$this->fieldtype = TRUE;
		$this->defaulttype = 'VARCHAR';
    }
	
	/**
	 * 字段相关属性参数
	 *
	 * @param	array	$value	值
	 * @return  string
	 */
	public function option($option) {

        $option['type'] = isset($option['type']) ? $option['type'] : 0;
        $option['code'] = isset($option['code']) ? $option['code'] : '';
        $option['file'] = isset($option['file']) ? $option['file'] : '';

        $str = '<select class="form-control" name="data[setting][option][file]"><option value=""> -- </option>';
        $files = dr_file_map(ROOTPATH.'config/myfield/', 1);
        if ($files) {
            foreach ($files as $t) {
                $t && strpos($t, '.php') !== 0 && $str.= '<option value="'.$t.'" '.($option['file'] == $t ? 'selected' : '').'> '.$t.' </option>';
            }
        }
        $str.= '</select>';

		return ['<div class="form-group">
                    <label class="col-md-2 control-label">'.dr_lang('自定义类型').'</label>
                    <div class="col-md-9">
                    <div class="mt-radio-inline">
                    <label class="mt-radio mt-radio-outline"><input type="radio" onclick="dr_diy_type(0)" value="0" name="data[setting][option][type]" '.(!$option['type'] ? 'checked' : '').'> '.dr_lang('代码模式').' <span></span></label>
                    <label class="mt-radio mt-radio-outline"><input type="radio" onclick="dr_diy_type(1)" value="1" name="data[setting][option][type]" '.($option['type'] ? 'checked' : '').'> '.dr_lang('文件模式').' <span></span></label>
                    </div>
                    </div>
                </div>
                <div class="form-group dr_type" id="dr_diy_type_0" style="display:none">
                    <label class="col-md-2 control-label">'.dr_lang('自定义代码').'</label>
                    <div class="col-md-9">
                    <textarea class="form-control" name="data[setting][option][code]" style="height:150px;width:80%;">'.$option['code'].'</textarea>
					<span class="help-block">'.dr_lang('将设计好的代码放到这里').'</span>
                    </div>
                </div>
                <div class="form-group dr_type" id="dr_diy_type_1" style="display:none">
                    <label class="col-md-2 control-label">'.dr_lang('自定义文件').'</label>
                    <div class="col-md-9">
                    <label>'.$str.'</label>
                    <span class="help-block">'.dr_lang('将设计好的文件上传到./config/myfield/目录之下').'</span>
                    </div>
                </div>
                <script>
                function dr_diy_type(i) {
                    $(".dr_type").hide();
                    $("#dr_diy_type_"+i).show();
                }
                dr_diy_type('.intval($option['type']).');
                </script>
				<div class="form-group">
                    <label class="col-md-2 control-label">'.dr_lang('默认值').'</label>
                    <div class="col-md-9">
                    <label><input id="field_default_value" type="text" class="form-control" size="20" value="'.$option['value'].'" name="data[setting][option][value]"></label>
					<label>'.$this->member_field_select().'</label>
					<span class="help-block">'.dr_lang('也可以设置会员表字段，表示用当前登录会员信息来填充这个值').'</span>
                    </div>
                </div>'.$this->field_type($option['fieldtype'], $option['fieldlength'])];
	}
	
	/**
	 * 字段入库值
	 */
	public function insert_value($field) {
		$data = \Phpcmf\Service::L('Field')->post[$field['fieldname']];
        is_array($data) && $data = dr_array2string($data);
		\Phpcmf\Service::L('Field')->data[$field['ismain']][$field['fieldname']] = $data;
	}
	
	/**
	 * 字段表单输入
	 *
	 * @param	string	$cname	字段
	 * @param	string	$value	值
	 * @return  string
	 */
    public function input($field, $value = 0) {

        // 字段禁止修改时就返回显示字符串
        if ($this->_not_edit($field, $value)) {
            return $this->show($field, $value);
        }

        // 字段显示名称
        $text = ($field['setting']['validate']['required'] ? '<span class="required" aria-required="true"> * </span>' : '').$field['name'];

        // 表单附加参数
		$attr = isset($field['setting']['validate']['formattr']) && $field['setting']['validate']['formattr'] ? $field['setting']['validate']['formattr'] : '';
		// 字段提示信息
		$tips = isset($field['setting']['validate']['tips']) && $field['setting']['validate']['tips'] ? '<span class="help-block" id="dr_'.$field['fieldname'].'_tips">'.$field['setting']['validate']['tips'].'</span>' : '';
		// 字段默认值
		$value = @strlen($value) ? $value : $this->get_default_value($field['setting']['option']['value']);

		if ($field['setting']['option']['type']) {
            // 文件类型
            $file = WEBPATH.'config/myfield/'.$field['setting']['option']['file'];
            if (is_file($file)) {
                $name = $field['fieldname'];
                require_once $file;
            } elseif (!$field['setting']['option']['file']) {
                $code = '<font color=red>没有选择文件，在字段属性中选择</font>';
            } else {
                $code = '<font color=red>文件（'.$file.'）不存在</font>';
            }
        } else {
            // 代码类型
            if ($field['setting']['option']['code']) {
                ob_start();
                $name = $field['fieldname'];
                $file = \Phpcmf\Service::V()->code2php($field['setting']['option']['code']);
                require_once $file;
                $code = ob_get_clean();
            } else {
                $code = '<font color=red>没有可用代码，在字段属性中添加</font>';
            }
        }
		return $this->input_format($field['fieldname'], $text, $code);
	}
	
}