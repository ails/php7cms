<?php namespace Phpcmf\Extend;

class Request extends \CodeIgniter\HTTP\IncomingRequest
{
    protected function parseRequestURI(): string
    {
        return '/'; // 这里要固定返回 / 确保php7cms自定义URL正常使用
    }
}