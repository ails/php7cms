<?php
namespace Phpcmf\Extend;


class View extends \CodeIgniter\View\View
{

    public function getData()
    {
        return \Phpcmf\Service::V()->get_data();
    }

}