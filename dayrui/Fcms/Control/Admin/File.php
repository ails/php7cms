<?php namespace Phpcmf\Admin;

// 文件操作控制器
class File extends \Phpcmf\Common
{
    protected $dir;
    protected $root_path;
    protected $backups_dir;
    protected $exclude_dir;
    protected $backups_path;

    public function __construct(...$params)
    {
        parent::__construct(...$params);
        exit("为了程序安全，不提供在线修改文件功能");
    }

    protected function _Image() {

    }

    protected function _List() {


    }

    protected function _Add() {


    }

    protected function _Edit() {


    }

    protected function _Del() {


    }

    protected function _Clear() {


    }

    /**
     * 文件图标
     */
    protected function _file_icon($file) {


    }

    /**
     * 安全目录
     */
    protected function _safe_path($string) {

    }

    /**
     * 目录扫描
     */
    protected function _map_file($dir) {

    }

    // 存储文件别名
    protected function _save_name_ini($file, $value) {


    }

    // 获取单个文件别名
    protected function _get_name_ini($file) {


    }

    // 获取本目录全部别名
    protected function _get_list_ini($path) {


    }

    // 格式化内容
    protected function _get_code($code, $ext) {

    }

}
