<?php namespace Phpcmf\Model;

// 系统缓存
class Cache extends \Phpcmf\Model
{

    // 更新缓存
    public function update_cache() {

        $site_cache = $this->table('site')->getAll();
        $module_cache = $this->table('module')->order_by('displayorder ASC,id ASC')->getAll();

        \Phpcmf\Service::M('site')->cache(0, $site_cache, $module_cache);

        // 全局缓存
        foreach (['auth','variable', 'urlrule', 'member' ] as $m) {
            \Phpcmf\Service::M($m)->cache();
        }

        // 按站点更新的缓存
        $cache = [
            'navigator' => '',
            'tag' => '',
            'linkage' => '',
            'form' => '',
            'block' => '',
        ];


        foreach ($site_cache as $t) {

            \Phpcmf\Service::M('table')->cache($t['id'], $module_cache);
            \Phpcmf\Service::M('module')->cache($t['id'], $module_cache);

            foreach ($cache as $m => $namespace) {
                \Phpcmf\Service::M($m, $namespace)->cache($t['id']);
            }
        }

        \Phpcmf\Service::M('menu')->cache();
        $this->update_data_cache();
        return;
    }

    // 重建索引
    public function update_search_index() {


        $site_cache = $this->table('site')->getAll();
        $module_cache = $this->table('module')->getAll();
        if (!$module_cache) {
            return;
        }

        foreach ($site_cache as $t) {
            foreach ($module_cache as $m ) {
                $table = dr_module_table_prefix($m['dirname'], $t['id']);
                // 判断是否存在表
                if (!$this->db->tableExists($table)) {
                    continue;
                }
                $this->db->table($table.'_search')->truncate();
                $this->db->table($table.'_search_index')->truncate();
            }
        }
        return;

    }

    // 更新数据
    public function update_data_cache() {

        // 清空系统缓存
        \Phpcmf\Service::L('cache')->init()->clean();

        // 清空文件缓存
        \Phpcmf\Service::L('cache')->init('file')->clean();

        // 递归删除文件
        dr_dir_delete(WRITEPATH.'html');
        dr_dir_delete(WRITEPATH.'temp');
        dr_dir_delete(WRITEPATH.'attach');
        dr_dir_delete(WRITEPATH.'template');


        return;
    }

    // 初始化后台菜单
    public function update_site_config() {


        \Phpcmf\Service::M('Menu')->init('admin');
        \Phpcmf\Service::L('Input')->system_log('初始化后台菜单');
        return;
    }

    // 复制目录
    private function _copy_dir($src, $dst) {

        $dir = opendir($src);
        if (!is_dir($dst)) {
            @mkdir($dst);
        }

        $src = rtrim($src, '/');
        $dst = rtrim($dst, '/');

        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if (is_dir($src . '/' . $file) ) {
                    dr_mkdirs($dst . '/' . $file);
                    $this->_copy_dir($src . '/' . $file, $dst . '/' . $file);
                    continue;
                } else {
                    if (is_file($dst . '/' . $file)) {
                        continue;
                    }
                    dr_mkdirs(dirname($dst . '/' . $file));
                    $rt = copy($src . '/' . $file, $dst . '/' . $file);
                    !$rt && $this->_error_msg('['.$dst . '/' . $file. '] 生成失败');
                }
            }
        }
        closedir($dir);
    }

    // 错误进度
    private function _error_msg($msg) {
        echo json_encode(dr_return_data(0, $msg));exit;
    }

}