<?php

/**
 *  通知动作注册配置
 *
 *  动作字符 => 动作名称
 *
 **/

return [

    'member_register'           => '用户注册',
    'member_login'              => '用户登录',
    'pay_success'               => '付款成功',
    'pay_admin'                 => '后台充值',
    'member_edit_password'      => '修改密码',
    'member_verify_group'       => '审核用户组',
    'member_verify_cash'        => '审核提现申请',
    'member_edit_group'         => '用户组变更',
    'member_edit_level'         => '用户组等级变更',


];