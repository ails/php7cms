<?php

/**
 * 菜单配置
 */


return [

    'admin' => [

        'home' => [
            'name' => '首页',
            'icon' => 'fa fa-home',
            'left' => [
                'home-my' => [
                    'name' => '我的面板',
                    'icon' => 'fa fa-home',
                    'link' => [
                        [
                            'name' => '后台首页',
                            'icon' => 'fa fa-home',
                            'uri' => 'home/main',
                        ],
                        [
                            'name' => '资料修改',
                            'icon' => 'fa fa-user',
                            'uri' => 'api/my',
                        ],
                        [
                            'name' => '系统更新',
                            'icon' => 'fa fa-refresh',
                            'uri' => 'cache/index',
                        ],
                        [
                            'name' => '程序升级',
                            'icon' => 'fa fa-cloud',
                            'uri' => 'update/index',
                        ],
                    ]
                ],
            ],
        ],


        'system' => [
            'name' => '系统',
            'icon' => 'fa fa-globe',
            'left' => [
                'system-wh' => [
                    'name' => '系统维护',
                    'icon' => 'fa fa-cog',
                    'link' => [
                        [
                            'name' => '系统环境',
                            'icon' => 'fa fa-cog',
                            'uri' => 'system/index',
                        ],
                        [
                            'name' => '系统缓存',
                            'icon' => 'fa fa-clock-o',
                            'uri' => 'system_cache/index',
                        ],
                        [
                            'name' => '系统提醒',
                            'icon' => 'fa fa-bell',
                            'uri' => 'notice/index',
                        ],
                        [
                            'name' => '数据字典',
                            'icon' => 'fa fa-database',
                            'uri' => 'db/index',
                        ],
                        [
                            'name' => '系统体检',
                            'icon' => 'fa fa-wrench',
                            'uri' => 'check/index',
                        ],
                    ]
                ],
                'system-log' => [
                    'name' => '日志管理',
                    'icon' => 'fa fa-calendar',
                    'link' => [
                        [
                            'name' => 'PHP错误',
                            'icon' => 'fa fa-bug',
                            'uri' => 'error_php/index',
                        ],
                        [
                            'name' => '系统错误',
                            'icon' => 'fa fa-shield',
                            'uri' => 'error/index',
                        ],
                        [
                            'name' => '操作日志',
                            'icon' => 'fa fa-calendar',
                            'uri' => 'system_log/index',
                        ],
                    ]
                ],
            ],
        ],

        'config' => [
            'name' => '设置',
            'icon' => 'fa fa-cogs',
            'left' => [
                'config-web' => [
                    'name' => '网站设置',
                    'icon' => 'fa fa-cog',
                    'link' => [
                        [
                            'name' => '网站设置',
                            'icon' => 'fa fa-cog',
                            'uri' => 'site_config/index',
                        ],
                        [
                            'name' => '手机设置',
                            'icon' => 'fa fa-mobile',
                            'uri' => 'site_mobile/index',
                        ],
                        [
                            'name' => '域名绑定',
                            'icon' => 'fa fa-globe',
                            'uri' => 'site_domain/index',
                        ],
                        [
                            'name' => '水印设置',
                            'icon' => 'fa fa-photo',
                            'uri' => 'site_watermark/index',
                        ],
                    ]
                ],
                'config-content' => [
                    'name' => '内容设置',
                    'icon' => 'fa fa-navicon',
                    'link' => [
                        [
                            'name' => '创建模块',
                            'icon' => 'fa fa-plus',
                            'uri' => 'module_create/index',
                        ],
                        [
                            'name' => '模块管理',
                            'icon' => 'fa fa-gears',
                            'uri' => 'module/index',
                        ],
                        [
                            'name' => '模块搜索',
                            'icon' => 'fa fa-search',
                            'uri' => 'module_search/index',
                        ],
                        [
                            'name' => '网站表单',
                            'icon' => 'fa fa-table',
                            'uri' => 'form/index',
                        ],
                    ]
                ],

                'config-seo' => [
                    'name' => 'SEO设置',
                    'icon' => 'fa fa-internet-explorer',
                    'link' => [
                        [
                            'name' => '站点SEO',
                            'icon' => 'fa fa-cog',
                            'uri' => 'seo_site/index',
                        ],
                        [
                            'name' => '模块SEO',
                            'icon' => 'fa fa-gears',
                            'uri' => 'seo_module/index',
                        ],
                        [
                            'name' => '内容SEO',
                            'icon' => 'fa fa-th-large',
                            'uri' => 'seo_content/index',
                        ],
                        [
                            'name' => '栏目SEO',
                            'icon' => 'fa fa-reorder',
                            'uri' => 'seo_category/index',
                        ],
                        [
                            'name' => '搜索SEO',
                            'icon' => 'fa fa-search',
                            'uri' => 'seo_search/index',
                        ],
                        [
                            'name' => 'URL规则',
                            'icon' => 'fa fa-link',
                            'uri' => 'urlrule/index',
                        ],
                    ]
                ],
            ],
        ],



        'content' => [
            'name' => '内容',
            'icon' => 'fa fa-th-large',
            'left' => [
                'content-module' => [
                    'name' => '内容管理',
                    'icon' => 'fa fa-th-large',
                    'link' => [
                        [
                            'name' => '栏目管理',
                            'icon' => 'fa fa-reorder',
                            'uri' => 'module_category/index',
                        ],
                    ]
                ],
                'content-form' => [
                    'name' => '网站表单',
                    'icon' => 'fa fa-table',
                    'link' => []
                ],
                'content-verify' => [
                    'name' => '内容审核',
                    'icon' => 'fa fa-edit',
                    'link' => [
                    ]
                ],
            ],
        ],

        'code' => [
            'name' => '扩展',
            'icon' => 'fa fa-code',
            'left' => [
                'code-html' => [
                    'name' => '内容扩展',
                    'icon' => 'fa fa-home',
                    'link' => [
                        [
                            'name' => '关键词库',
                            'icon' => 'fa fa-tags',
                            'uri' => 'tag/index',
                        ],
                        [
                            'name' => '联动菜单',
                            'icon' => 'fa fa-columns',
                            'uri' => 'linkage/index',
                        ],
                        [
                            'name' => '自定义链接',
                            'icon' => 'fa fa-link',
                            'uri' => 'navigator/index',
                        ],
                        [
                            'name' => '自定义资料',
                            'icon' => 'fa fa-th-large',
                            'uri' => 'block/index',
                        ],
                        [
                            'name' => '自定义变量',
                            'icon' => 'fa fa-code',
                            'uri' => 'myvar/index',
                        ],
                    ]
                ],
            ],
        ],








    ],


    'member' => [





    ],
];