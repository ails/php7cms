<?php

/**
 * PHP7CMS主程序
 */

declare(strict_types=1);
header('Content-Type: text/html; charset=utf-8');

error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_STRICT);
define('IS_DEV', 0);

define('ROOTPATH', dirname(__FILE__).'/');

define('FCPATH', dirname(__FILE__).'/dayrui/');

define('WRITEPATH', ROOTPATH.'cache/');

define('WEBPATH', ROOTPATH);

!defined('SELF') && define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

!defined('IS_API') && define('IS_API', isset($_GET['s']) && $_GET['s'] == 'api');

!defined('IS_ADMIN') && define('IS_ADMIN', FALSE);


require FCPATH.'Fcms/Init.php';