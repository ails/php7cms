function dynamicLoadCss(url) {
    var head = document.getElementsByTagName('head')[0];
    var link = document.createElement('link');
    link.type='text/css';
    link.rel = 'stylesheet';
    link.href = url;
    head.appendChild(link);
}
function dynamicLoadJs(url, callback) {
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;
    if(typeof(callback)=='function'){
        script.onload = script.onreadystatechange = function () {
            if (!this.readyState || this.readyState === "loaded" || this.readyState === "complete"){
                callback();
                script.onload = script.onreadystatechange = null;
            }
        };
    }
    head.appendChild(script);
}

dynamicLoadCss(assets_path+'global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
dynamicLoadCss(assets_path+'global/css/plugins.min.css');
dynamicLoadCss(assets_path+'global/css/common.css');
dynamicLoadCss(assets_path+'font-awesome/css/font-awesome.css');
dynamicLoadCss(assets_path+'global/plugins/simple-line-icons/simple-line-icons.min.css');
dynamicLoadCss(assets_path+'global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css');
dynamicLoadCss(assets_path+'layouts/layout/css/themes/default.min.css');
dynamicLoadCss(assets_path+'layouts/layout/css/custom.css');

dynamicLoadJs(assets_path+'global/plugins/jquery-ui/jquery-ui.min.js');
dynamicLoadJs(assets_path+'');
dynamicLoadJs(assets_path+'global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
dynamicLoadJs(assets_path+'global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
dynamicLoadJs(assets_path+'global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js');
dynamicLoadJs(assets_path+'js/jquery.cookie.js');